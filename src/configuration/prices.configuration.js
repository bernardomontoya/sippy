const PriceConfiguration = {
    currencySymbol: "$",
    prices: [
        {
            title: "Free",
            price: "0",
            cents: "00",
            description: "For small teams or unlimited evaluation",
            billedAnnuallyLabel: "",
            billedMonthlyLabel: "",
            hasDetails: true,
            detailsLabel: "Details",
            hasAction: true,
            actionLabel: "Create a new team",
            priceIncludesLabel: "All of these great features:",
            color: "#8974b9",
            textColor: "#FFF",
            features: [
                {
                    feature: "Search and browse 10k most recent messages"
                },
                {
                    feature: "10 service integrations"
                }
                ,
                {
                    feature: "Free native apps for iOS, Android, Mac Desktop & Windows Desktop"
                }
            ]
        },
        {
            title: "Standard",
            price: "6",
            cents: "67",
            description: "",
            billedAnnuallyLabel: "per user per month billed annually",
            billedMonthlyLabel: "$8 billed monthly",
            hasDetails: true,
            detailsLabel: "Details",
            hasAction: true,
            actionLabel: "Buy Standard",
            priceIncludesLabel: "Everything in Free, and:",
            color: "#4cb992",
            textColor: "#FFF",
            features: [
                {
                    feature: "Search and browse 10k most recent messages"
                },
                {
                    feature: "10 service integrations"
                }
                ,
                {
                    feature: "Free native apps for iOS, Android, Mac Desktop & Windows Desktop"
                }
            ]
        },
        {
            title: "Plus",
            price: "12",
            cents: "50",
            description: "",
            billedAnnuallyLabel: "per user per month billed annually",
            billedMonthlyLabel: "$15 billed monthly",
            hasDetails: true,
            detailsLabel: "Details",
            hasAction: true,
            actionLabel: "Buy Plus",
            priceIncludesLabel: "Everything in Standard, and:",
            color: "#4d99e2",
            textColor: "#FFF",
            features: [
                {
                    feature: "SAML-based single sign-on(SSO)"
                },
                {
                    feature: "Compliance Exports of all message history"
                }
                ,
                {
                    feature: "Support for external message and archival solutions"
                }
                ,
                {
                    feature: "99.99% guaranteed uptime SLA"
                }
                ,
                {
                    feature: "24/7 support with 4 hour response time"
                }
                ,
                {
                    feature: "User provisioning"
                }
            ]
        },
        {
            title: "Enterprise",
            price: "12",
            cents: "50",
            description: "",
            billedAnnuallyLabel: "per user per month billed annually",
            billedMonthlyLabel: "",
            hasDetails: true,
            detailsLabel: "Details",
            hasAction: true,
            actionLabel: "Contact Us",
            priceIncludesLabel: "Everything in Plus, and:",
            color: "#f0b57d",
            textColor: "#FFF",
            features: [
                {
                    feature: "Federation across multiple teams with a unified team directory"
                },
                {
                    feature: "Compliance Exports of all message history"
                }
                ,
                {
                    feature: "Support for external message and archival solutions"
                }
                ,
                {
                    feature: "99.99% guaranteed uptime SLA"
                }
                ,
                {
                    feature: "24/7 support with 4 hour response time"
                }
                ,
                {
                    feature: "User provisioning"
                }
            ]
        }
        
    ]
}

export default PriceConfiguration;