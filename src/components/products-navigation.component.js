import React from 'react';
import SVG from './svg.component';
import styled from 'styled-components';
import { ReactComponent as MediaGateway } from '../assets/svg/media-gateway.svg';
import { ReactComponent as SoftSwitch } from '../assets/svg/softswitch.svg';
import Redundancy from '../assets/images/techr.png';
import SippyImage from './image.component';

const Nav = styled.div`
    width: 100%;
    padding: 0 40px;
    display: flex;
    flex-direction: column;
    align-items: center;
`
const NavContainer = styled.div`
    display: flex;
    flex-direction: row;
`

export default function ProductsNav(props) {
    const currentProduct = props.product;
    const NavElementContainer = styled.div`
        width: 100px;
        padding: 20px;
        & svg {
            opacity: 0.4;
            transition: opacity .2s ease;
        }
        & img {
            opacity: 0.4;
            transition: opacity .2s ease;
        }
        &.active svg {
            opacity: 1;
        }
        &.active img {
            opacity: 1;
        }
    `
    return(
        <Nav>
            <NavContainer className="sippy-container justify-center">
                <NavElementContainer className={ currentProduct === 'media-gateway' ? 'active': '' } >
                    <a href={'/products/media-gateway'} variant="outlined" size="small">
                        <SVG element={MediaGateway} width={100} />
                    </a>
                </NavElementContainer>
                <NavElementContainer className={ currentProduct === 'softswitch' ? 'active': '' }>
                    <a href={'/products/softswitch'} variant="outlined" size="small">
                        <SVG element={SoftSwitch} width={100} />
                    </a>
                </NavElementContainer>
                <NavElementContainer className={ currentProduct === 'redundancy' ? 'active': '' }>
                    <a href={'/products/redundancy'} variant="outlined" size="small">
                        <img src={Redundancy} alt="Redundancy"/>
                    </a>
                </NavElementContainer>
            </NavContainer>
        </Nav>
    )
}