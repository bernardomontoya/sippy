import React from 'react';
import styled from 'styled-components';
import Title from './title.component';
import Paragraph from './paragraph.component';
import SippyButton from './sippy-button.component';
import Footer from './footer.component';
import { NavLink } from 'react-router-dom';

const Container = styled.div`
    width: 100%;
    height: auto;
    padding: 60px 40px 80px;
    display: flex;
    flex-direction: column;
    align-items: center;
    & .sippy-contact-us-text-container {
        margin-bottom: 40px;
    }
`

export default function ContactUs() {
    return(
        <>
        <Container>
            <div className="sippy-container sippy-container-column">
                <Title title="Contact us" subTitle="We are ready to start"/>
                <div className="sippy-contact-us-text-container">
                    <Paragraph size="medium" text="Sippy Software is one of the leading developers of next-generation technology for providers of Internet Telephony (VoIP) troughout the world. We provide several SIP-based products and solutions for VoIP carriers."/>
                    <Paragraph size="medium" text="Sippy Software also offers consulting, custom software development and support for VoIP-related systems."/>
                    <Paragraph size="medium" text="8100 Winston Street Burnaby, BC, Canada V5A 2H5"/>
                    <Paragraph size="medium" text="Toll Free: +1 855 747 7779"/>
                    <Paragraph size="medium" text="Canada: +1 778 783 0474"/>
                    <Paragraph size="medium" text="Support: 24 x 7 - 365 days"/>
                </div>
                <NavLink to="/form" exact className="">
                <SippyButton label="Free demo request" size="large"/>
                </NavLink>
            </div>
        </Container>
        <Footer/>
        </>
    )
}