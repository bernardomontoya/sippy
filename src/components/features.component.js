import React from 'react';
import styled from 'styled-components';
import Title from './title.component';
import Feature from './feature.component';
import { device } from './devices';
import IntroSection from './intro-section.component';
import AboutVideo from '../assets/videos/features.mp4';
import Footer from './footer.component';

const Container = styled.div`
    width: 100%;
    padding: 0 40px 80px;
    display: flex;
    flex-direction: column;
    align-items: center;
    & .sippy-features-container {
        width: 100%;
        display: flex;
        flex-direction: row;
        margin-bottom: 40px;
        @media (max-width: ${device.mobileL}) {
            flex-direction: column;
            width: 100%;
            & > div {
                width: 100%;
                margin-bottom: 40px;
                margin-right: 0;
            }
        }
    }
`

export default function Features() {
    return(
        <>
        <Container>
            <div className="sippy-container sippy-container-column">
                <IntroSection type="video" hasButton={false} video={AboutVideo} />
                <div className="sippy-features-container">
                    <Feature
                        title="Flexible Routing Tools"
                        readMoreLabel="Read more"
                        route="flexible-routing-tools"
                        icon="flexible-routing-tools"
                    />
                    <Feature
                        title="Real-Time Monitoring"
                        readMoreLabel="Read more"
                        route="real-time"
                        icon="Real-Time"
                    />
                    <Feature
                        title="Business Reporting"
                        readMoreLabel="Read more"
                        route="business-reporting"
                        icon="Business-Reporting"
                    />
                    <Feature
                        title="Billing Management"
                        readMoreLabel="Read more"
                        route="billing-management"
                        icon="Billing-Management"
                    />
                    <Feature
                        title="System Administration"
                        readMoreLabel="Read more"
                        route="system-administration"
                        icon="System-Administration"
                    />
                </div>
                <div className="sippy-features-container">
                    <Feature
                        title="XML-RPC API Control"
                        readMoreLabel="Read more"
                        route="xml-rpc"
                        icon="xml-rpc"
                    />
                    <Feature
                        title="Support and Security"
                        readMoreLabel="Read more"
                        route="support-and-security"
                        icon="Support-and-Security"
                    />
                    <Feature
                        title="Sippy Environments"
                        readMoreLabel="Read more"
                        route="sippy-environments"
                        icon="Sippy-Environments"
                    />
                    <Feature
                        title="Calling Card Module"
                        readMoreLabel="Read more"
                        route="calling-card-module"
                        icon="Calling-Card-Module"
                    />
                    <Feature
                        title="Web Callback Module"
                        readMoreLabel="Read more"
                        route="web-callback-module"
                        icon="Web-Callback-Module"
                    />
                </div>
                <div className="sippy-features-container">
                    <Feature
                        title="DID Management"
                        readMoreLabel="Read more"
                        route="did-management"
                        icon="DID-Management"
                    />
                    <Feature
                        title="Do not call list"
                        readMoreLabel="Read more"
                        route="do-not-call-list"
                        icon="DO-NOT-CALL-LIST"
                    />
                    <Feature
                        title="Regulatory Features"
                        readMoreLabel="Read more"
                        route="turkish-regulatory-features"
                        icon="Turkish-Regulatory-Features"
                    />
                    <Feature
                        title="List of Supported Currencies"
                        readMoreLabel="Read more"
                        route="list-of-supported-currencies"
                        icon="List-of-Supported-Currencies"
                    />
                </div>
                <div className="sippy-features-container">

                </div>
            </div>
        </Container>
        <Footer/>
        </>
    )
}