import React, { useState } from "react";
import { useMediaQuery } from "react-responsive";
import Popover from "@material-ui/core/Popover";
import styled from "styled-components";
import Title from "./title.component";
import Paragraph from "./paragraph.component";
import SVG from "./svg.component";
import SippyImage from "./image.component";
import Footer from "./footer.component";
import { ReactComponent as MediaGateway } from "../assets/svg/media-gateway.svg";
import RedundancyImage from "../assets/images/redundancy.png";
import ProductsNav from "./products-navigation.component";

/*<<<<<<< HEAD*/
import Redundancylo from "../assets/images/techr.png";
/*=======*/
import Redundancy from "../assets/images/techr.png";
import { ReactComponent as Product1 } from "../assets/svg/product1.svg";
import { ReactComponent as Product2 } from "../assets/svg/product2.svg";
import { ReactComponent as Product3 } from "../assets/svg/product3.svg";
import { ReactComponent as Product4 } from "../assets/svg/product4.svg";
import { ReactComponent as Product5 } from "../assets/svg/product5.svg";
import { ReactComponent as Product6 } from "../assets/svg/product6.svg";
import { ReactComponent as Product7 } from "../assets/svg/product7.svg";
import { ReactComponent as SoftSwitch } from "../assets/svg/softswitch.svg";

const Container = styled.div`
  width: 100%;
  padding: 60px 40px 80px;
  display: flex;
  flex-direction: column;
  align-items: center;
  & img {
    margin-bottom: 60px;
  }
  .sippy-structure {
    width: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    & .sippy-router-container {
      .sippy-router {
        border: 12px solid #333;
        border-radius: 32px;
        display: flex;
        flex-direction: row;
        padding: 12px;
        cursor: pointer;
        width: 120px;
        & .sippy-router-icon {
          & > div {
            height: 100%;
          }
        }
        & .sippy-router-text {
          width: 80%;
          color: #fff;
          display: flex;
          flex-direction: column;
          justify-content: center;
          font-size: 22px;
          & span {
            width: 100%;
          }
        }
      }
      .sippy-router-bottom {
        margin-top: -18px;
      }
      .sippy-router-bottom-hor-line {
        border: 8px solid #333;
        border-radius: 8px;
      }
      .sippy-router-bottom-ver-line {
        width: 100%;
        height: 50px;
        display: flex;
        flex-direction: row;
        .sippy-router-bottom-left-line {
          width: 100%;
          height: 100%;
          border-right: 8px solid #333;
        }
        .sippy-router-bottom-right-line {
          width: 100%;
          height: 100%;
          border-left: 8px solid #333;
        }
      }
    }
  }
`;
const Image = styled(SippyImage)`
  margin-bottom: 40px;
`;
const CustomImage = styled.img`
  width: ${(props) => props.width}%;
  padding-bottom: 60px;
`;

function ShowProduct(product, activeProduct, setActiveProduct) {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const isTabletOrMobile = useMediaQuery({ maxWidth: 1224 });
  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;
  if (product === "media-gateway") {
    return (
      <>
        <ProductsNav product={product} />
        <Container>
          <Title title="Sippy Media Gateway (SMG)" />
          <Paragraph
            size="medium"
            text="High-performance software proxy that brings further control and efficiency to your VoIP network."
          />
          <SVG element={MediaGateway} width={isTabletOrMobile ? 100 : 25} />
          <Paragraph
            size="medium"
            text="Sippy Media Gateway (SMG) is the most powerful RTP-Proxy based media gateway solution on the market. Designed and built by Sippy Software, the SMG license provides a rapid return on investment, optimizes the distribution of SIP traffic, enhances overall network performance and delivers unlimited channel capacity through clustering technology."
          />
          <Paragraph
            size="medium"
            text="As with all Sippy products, the Sippy Media Gateway provides a clear distributed network migration and scalability path. Clients can continue to expand their existing business using the bundled, Internal SMG solutions, migrate to a hosted media gateway solution, or deploy a complete clustered and/or distributed SMG solution using the External SMG solutions."
          />
        </Container>
        <Footer />
      </>
    );
  } else if (product === "redundancy") {
    return (
      <>
        <ProductsNav product={product} />
        <Container>
          <Title title="Redundancy" />
          <CustomImage
            src={Redundancy}
            alt="Redundancy"
            width={isTabletOrMobile ? 100 : 30}
          />
          <Title title="High Availability" />
          <Paragraph
            size="medium"
            text="The Sippy Standby license is a secondary instance of the Sippy Softswitch that can be promoted to primary production server in the event of failure, adding a functional layer of network redundancy to enhance the operational stability of all voice network architectures. The Standby server can also be promoted to the primary role during scheduled maintenance, allowing for continuity of service. Achieve active failover, passive failover or data replication for reporting purposes, through the implementation of the Sippy Standby license."
          />
          <Paragraph
            size="medium"
            text="Streaming data replication ensures the mirroring of data to a redundant Sippy Database, delivering data securely to a live standby server. Sippy employs Slony PostgreSQL for its streaming data replication, resulting in a more flexible and stable mirroring of configurations and call accounting data for your business continuity in the event of failure."
          />
          <Image
            src={RedundancyImage}
            alt="redundancy"
            width={isTabletOrMobile ? 100 : 60}
          />
          <Paragraph
            size="medium"
            text="The Sippy Standby server can be configured with a degree of flexibility to deliver redundancy benefits to different business requirements. When the issue has been resolved and the primary server becomes available again, any changes to the standby server's copies of databases must be restored back to the primary server. A reversion switchover procedure is performed by Sippy Support, returning original network architecture to normal."
          />
        </Container>
        <Footer />
      </>
    );
  } else if (product === "softswitch") {
    const handleClick = (event, activeProduct) => {
      setAnchorEl(event.currentTarget);
      setActiveProduct(activeProduct);
    };

    const handleClose = () => {
      setAnchorEl(null);
    };
    const GetBottomInfo = () => {
      let title = "";
      let description = "";
      switch (activeProduct) {
        case "Comprehensive Routing":
          title = "Comprehensive Routing";
          description =
            "Various key features and settings allow users to configure routing options that work. From Least Cost Routing, to Quality Based Routing, to On-Net Routing, to Priority Routing, Sippy delivers the level of configurability that will help your business remain flexible to what your business strategy requires.";
          break;
        case "20 Point Billing":
          title = "20 Point Billing";
          description =
            "Our billing engine provides a flexible and dynamic suite of tools to initiate new services, provide comprehensive pricing solutions for both pre-paid, post-paid clients, wholesale, carrier, and call center solutions providers. In addition, the Sippy customer Invoice Template tool allows clients to build unique, custom invoice templates to match their business branding.";
          break;
        case "Scalability":
          title = "Scalable";
          description =
            "Business success requires the ability to scale. Our flexible licensing models allow the technology we deliver to keep up with your business needs. Through our monthly rental, full purchase and Flex purchase options, you can easily scale your softswitch from 100CC all the way to 15,000CC to keep up with customer demand and business growth. The Sippy Media Gateway allows you to scale your network further by addressing issues of Our partnership status with top data centers also allows us to guarantee that as a company we can scale to meet your demands.";
          break;
        case "Flexible":
          title = "Flexible";
          description =
            "We recognize that no two businesses are alike so we provide licensing options that provide flexibility in terms of ownership. With our Flex licensing option, you can “rent” a license and purchase the license when your business permits. The best part of the Flex licensing option is that it guarantees that we will credit a portion of your Flex “rental” payments towards the purchase of your license.";
          break;
        case "Robust":
          title = "Robust";
          description =
            "Our developers are professionals that understand SIP. We choose to provision our softswitch on the very stable and reliable FreeBSD operating system because of the various features that it delivers making any system based on its technology both robust and secure. Our code is tried and tested and before every release our engineers put each component through rigorous paces to make sure that our reputation for reliability is not compromised.";
          break;
        case "SBC":
          title = "SBC";
          description =
            "Sippy delivers a built-in Session Border Controller (SBC) which allows you and your customers to connect, and terminate calls without compromising security, automatically detecting VoIP threats and taking action. Like all our technology, we develop and maintain the SBC internally ensuring that functionality is always optimized.";
          break;
        case "Built-in RTP Proxy":
          title = "Built-in RTP Proxy";
          description =
            "The optimized RTP media proxy is built into the Sippy Softswitch resulting in optimized traffic flow, NAT Traversal, and topology masking, simplifying VoIP networks.";
          break;
        default:
          title = "";
          description = "";
          break;
      }
      return (
        <>
          <h2 className="white mb3 f3-ns">{title}</h2>
          <p className="white f4-ns">{description}</p>
        </>
      );
    };
    return (
      <>
        <ProductsNav product={product} />
        <Container>
          <Title title="Softswitch" />
          <SVG element={SoftSwitch} width={isTabletOrMobile ? 100 : 26} />
          <Title title="Our Technology" />
          <div className="sippy-structure mt4 mb0 mb5-ns">
            <div className="sippy-router-container flex flex-column flex-row-ns">
              <div
                className="sippy-router mr4-ns mb3 mb0-ns"
                onClick={(event) => handleClick(event, "Comprehensive Routing")}
              >
                <div className="sippy-router-icon">
                  <SVG element={Product1} width={100} />
                </div>
              </div>

              <div
                className="sippy-router mr4-ns mb3 mb0-ns"
                onClick={(event) => handleClick(event, "20 Point Billing")}
              >
                <div className="sippy-router-icon">
                  <SVG element={Product2} width={100} />
                </div>
              </div>
              <div
                className="sippy-router mr4-ns mb3 mb0-ns"
                onClick={(event) => handleClick(event, "Scalability")}
              >
                <div className="sippy-router-icon">
                  <SVG element={Product3} width={100} />
                </div>
              </div>
              <div
                className="sippy-router mr4-ns mb3 mb0-ns"
                onClick={(event) => handleClick(event, "Flexible")}
              >
                <div className="sippy-router-icon">
                  <SVG element={Product4} width={100} />
                </div>
              </div>
              <div
                className="sippy-router mr4-ns mb3 mb0-ns"
                onClick={(event) => handleClick(event, "Robust")}
              >
                <div className="sippy-router-icon">
                  <SVG element={Product5} width={100} />
                </div>
              </div>
              <div
                className="sippy-router mr4-ns mb3 mb0-ns"
                onClick={(event) => handleClick(event, "SBC")}
              >
                <div className="sippy-router-icon">
                  <SVG element={Product6} width={100} />
                </div>
              </div>
              <div
                className="sippy-router"
                onClick={(event) => handleClick(event, "Built-in RTP Proxy")}
              >
                <div className="sippy-router-icon">
                  <SVG element={Product7} width={100} />
                </div>
              </div>
            </div>
          </div>

          {activeProduct !== undefined &&
          activeProduct !== null &&
          activeProduct !== "" ? (
            <div className="sippy-container">
              <div>
                {isTabletOrMobile ? (
                  <Popover
                    id={id}
                    open={open}
                    anchorEl={anchorEl}
                    onClose={handleClose}
                    anchorOrigin={{
                      vertical: "bottom",
                      horizontal: "center",
                    }}
                    transformOrigin={{
                      vertical: "top",
                      horizontal: "center",
                    }}
                  >
                    <GetBottomInfo />
                  </Popover>
                ) : (
                  <GetBottomInfo />
                )}
              </div>
            </div>
          ) : null}
        </Container>
        <Footer />
      </>
    );
  } else {
    return (
      <>
        <Container>
          <p>El producto que deseas visualizar no existe</p>
        </Container>
        <Footer />
      </>
    );
  }
}

export default function Product(props) {
  const product = props.match.params.id;
  const [activeProduct, setActiveProduct] = useState("");
  return ShowProduct(product, activeProduct, setActiveProduct);
}
