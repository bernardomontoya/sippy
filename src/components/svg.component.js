import React from 'react';
import styled from 'styled-components';

export default function SVG(props) {
    const Element = props.element;
    const width = props.width;
    const margintop = "0px";
    const marginbottom = "0px";
    console.log(Element);
    const Container = styled.div`
        width: 100%;
        display: flex;
        flex-direction: column;
        align-items: center;
        .svg-container {
            width: ${width !== undefined ? width : 100}%;
            & svg {
                width: 100%;
                height: auto;
            }
        }
        & .svg-softswitch {
            margin-top: -152px;
            margin-bottom: -62px;
        }
    `
    return(
        <Container>
            <div className={width === 26 ? 'svg-container svg-softswitch' : "svg-container"}>
                <Element/>
            </div>
        </Container>
    )
}