import React from 'react';
import styled from 'styled-components';
import { device } from './devices';

const StyledTitle = styled.h1`
    font-size: 34px;
    color: #f8941e;
    @media (max-width: ${device.mobileL}) {
        font-size: 32px;
    }
`
const TitleContainer = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    text-align: center;
    margin-bottom: 40px;
    & span {
        color: #ec8d2f;
        margin-top: 8px;
        font-size: 20px;
    }
`

export default function Title(props) {
    const title = props.title;
    const subTitle = props.subTitle;
    return(
        <TitleContainer>
            <StyledTitle>{title}</StyledTitle>
            {
                subTitle !== "" && subTitle !== undefined
                ?
                <span>{subTitle}</span>
                :
                null
            }
        </TitleContainer>
    )
}