import React from 'react';
import styled from 'styled-components';
import Title from './title.component';
import Paragraph from './paragraph.component';
import PriceCard from './price.component';
import PriceConfiguration from '../configuration/prices.configuration';
import SippyImage from './image.component';
import { device } from './devices';
import Pricing from '../assets/images/pricing.jpg';
import Pricing2 from '../assets/images/pricing2.jpg';
import Pricing3 from '../assets/images/pricing3.jpg';
import Footer from './footer.component';

// styles
const PricesContainer = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: center;
    
    @media (max-width: ${device.mobileL}) {
        flex-direction: column;
        & > div {
            margin-bottom: 40px;
        }
    }
`
const Container = styled.div`
    width: 100%;
    padding: 60px 40px 80px;
    display: flex;
    flex-direction: column;
    align-items: center;
    & .prices-container {
        width: 100%;
        display: flex;
        flex-direction: row;
        & .price-container {
            margin-right: 32px;
            width: 100%;
            &:last-child {
                margin-right: 0;
            }
            .price-table {
                background: #2f2c2e;
                display: flex;
                flex-direction: column;
                border: 1px solid #FFF;
                & .price-table-header {
                    display: flex;
                    width: 100%;
                    border-bottom: 1px solid #FFF;
                    & .price-table-header-item {
                        width: 100%;
                        color: #FFF;
                        text-align: center;
                        font-weight: 600;
                        padding: 12px;
                        border-right: 1px solid #FFF;
                        &:last-child {
                            border-right: none;
                        }
                    }
                }
                & .price-table-body {
                    width: 100%;
                    display: flex;
                    flex-direction: column;
                    & .price-table-body-row {
                        display: flex;
                        flex-direction: row;
                        width: 100%;
                        cursor: pointer;
                        transition: .2s all ease;
                        &:nth-child(even) {
                            background: #dad9d6;
                        }
                        &:nth-child(odd) {
                            background: #f6f6f6;
                        }
                        &:hover {
                            background: #f8941e;
                        }
                        & .price-table-body-row-item {
                            width: 100%;
                            color: #2f2b2e;
                            text-align: center;
                            font-weight: 500;
                            padding: 12px;
                            transition: .2s all ease;
                        }
                    }
                }
            }
        } 
    } 

`
// helper functions
const BuildPricesConfiguration = PriceConfiguration.prices.map(configuration => {
    return(
        <PriceCard currencySymbol={PriceConfiguration.currencySymbol} configuration={configuration} />
    )
})

export default function ComparePrices() {
    return(
        <>
        <Container>
            <div className="sippy-container sippy-container-column">
                <Title title="Compare prices"/>
                <Paragraph size="medium" text="Value is critical, while our Softswitch is loaded with features and boasts security and stability as its core foundation, it does not cost as much as you would expect. Our company believes in fair value so we price our solutions to address market needs in both products and price.  The result of our fair pricing practices: customers all over the world who constantly recommend our Softswitch to their peers."/>
                <div className="prices-container">
                    <div className="price-container">   
                        <Title title="Shared Hosting"/>
                        <div className="price-table">
                            <div className="price-table-header">
                                <div className="price-table-header-item">
                                    <span>Capacity</span>
                                </div>
                                <div className="price-table-header-item">
                                    <span>Monthly Fee</span>
                                </div>
                                <div className="price-table-header-item">
                                    <span>CPS</span>
                                </div>
                            </div>
                            <div className="price-table-body">
                                <div className="price-table-body-row">
                                    <div className="price-table-body-row-item">
                                        <span>100</span>
                                    </div>
                                    <div className="price-table-body-row-item price-table-body-row-item-price">
                                        <span>$195.00</span>
                                    </div>
                                    <div className="price-table-body-row-item">
                                        <span>15</span>
                                    </div>
                                </div>
                                <div className="price-table-body-row">
                                    <div className="price-table-body-row-item">
                                        <span>250</span>
                                    </div>
                                    <div className="price-table-body-row-item price-table-body-row-item-price">
                                        <span>$295.00</span>
                                    </div>
                                    <div className="price-table-body-row-item">
                                        <span>30</span>
                                    </div>
                                </div>
                                <div className="price-table-body-row">
                                    <div className="price-table-body-row-item">
                                        <span>500</span>
                                    </div>
                                    <div className="price-table-body-row-item price-table-body-row-item-price">
                                        <span>$495.00</span>
                                    </div>
                                    <div className="price-table-body-row-item">
                                        <span>45</span>
                                    </div>
                                </div>
                                <div className="price-table-body-row">
                                    <div className="price-table-body-row-item">
                                        <span>1000</span>
                                    </div>
                                    <div className="price-table-body-row-item price-table-body-row-item-price">
                                        <span>$695.00</span>
                                    </div>
                                    <div className="price-table-body-row-item">
                                        <span>60</span>
                                    </div>
                                </div>
                                <div className="price-table-body-row">
                                    <div className="price-table-body-row-item">
                                        <span>2000</span>
                                    </div>
                                    <div className="price-table-body-row-item price-table-body-row-item-price">
                                        <span>$995.00</span>
                                    </div>
                                    <div className="price-table-body-row-item">
                                        <span>75</span>
                                    </div>
                                </div>
                                <div className="price-table-body-row">
                                    <div className="price-table-body-row-item">
                                        <span>4000</span>
                                    </div>
                                    <div className="price-table-body-row-item price-table-body-row-item-price">
                                        <span>$1595.00</span>
                                    </div>
                                    <div className="price-table-body-row-item">
                                        <span>90</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="price-container">
                        <Title title="Flex Costumer Hosted"/>
                        <div className="price-table">
                            <div className="price-table-header">
                                <div className="price-table-header-item">
                                    <span>Capacity</span>
                                </div>
                                <div className="price-table-header-item">
                                    <span>CPS</span>
                                </div>
                                <div className="price-table-header-item">
                                    <span>FLEX Monthly Fee</span>
                                </div>
                            </div>
                            <div className="price-table-body">
                                <div className="price-table-body-row">
                                    <div className="price-table-body-row-item">
                                        <span>2000</span>
                                    </div>
                                    <div className="price-table-body-row-item price-table-body-row-item-price">
                                        <span>75</span>
                                    </div>
                                    <div className="price-table-body-row-item">
                                        <span>$476.00</span>
                                    </div>
                                </div>
                                <div className="price-table-body-row">
                                    <div className="price-table-body-row-item">
                                        <span>3000</span>
                                    </div>
                                    <div className="price-table-body-row-item price-table-body-row-item-price">
                                        <span>90</span>
                                    </div>
                                    <div className="price-table-body-row-item">
                                        <span>$576.00</span>
                                    </div>
                                </div>
                                <div className="price-table-body-row">
                                    <div className="price-table-body-row-item">
                                        <span>4000</span>
                                    </div>
                                    <div className="price-table-body-row-item price-table-body-row-item-price">
                                        <span>105</span>
                                    </div>
                                    <div className="price-table-body-row-item">
                                        <span>$676.00</span>
                                    </div>
                                </div>
                                <div className="price-table-body-row">
                                    <div className="price-table-body-row-item">
                                        <span>5000</span>
                                    </div>
                                    <div className="price-table-body-row-item price-table-body-row-item-price">
                                        <span>120</span>
                                    </div>
                                    <div className="price-table-body-row-item">
                                        <span>$776.00</span>
                                    </div>
                                </div>
                                <div className="price-table-body-row">
                                    <div className="price-table-body-row-item">
                                        <span>6000</span>
                                    </div>
                                    <div className="price-table-body-row-item price-table-body-row-item-price">
                                        <span>135</span>
                                    </div>
                                    <div className="price-table-body-row-item">
                                        <span>$876.00</span>
                                    </div>
                                </div>
                                <div className="price-table-body-row">
                                    <div className="price-table-body-row-item">
                                        <span>7000</span>
                                    </div>
                                    <div className="price-table-body-row-item price-table-body-row-item-price">
                                        <span>150</span>
                                    </div>
                                    <div className="price-table-body-row-item">
                                        <span>$976.00</span>
                                    </div>
                                </div>
                                <div className="price-table-body-row">
                                    <div className="price-table-body-row-item">
                                        <span>8000</span>
                                    </div>
                                    <div className="price-table-body-row-item price-table-body-row-item-price">
                                        <span>165</span>
                                    </div>
                                    <div className="price-table-body-row-item">
                                        <span>$1076.00</span>
                                    </div>
                                </div>
                                <div className="price-table-body-row">
                                    <div className="price-table-body-row-item">
                                        <span>9000</span>
                                    </div>
                                    <div className="price-table-body-row-item price-table-body-row-item-price">
                                        <span>180</span>
                                    </div>
                                    <div className="price-table-body-row-item">
                                        <span>$1176.00</span>
                                    </div>
                                </div>
                                <div className="price-table-body-row">
                                    <div className="price-table-body-row-item">
                                        <span>10000</span>
                                    </div>
                                    <div className="price-table-body-row-item price-table-body-row-item-price">
                                        <span>195</span>
                                    </div>
                                    <div className="price-table-body-row-item">
                                        <span>$1276.00</span>
                                    </div>
                                </div>
                                <div className="price-table-body-row">
                                    <div className="price-table-body-row-item">
                                        <span>11000</span>
                                    </div>
                                    <div className="price-table-body-row-item price-table-body-row-item-price">
                                        <span>195</span>
                                    </div>
                                    <div className="price-table-body-row-item">
                                        <span>$1376.00</span>
                                    </div>
                                </div>
                                <div className="price-table-body-row">
                                    <div className="price-table-body-row-item">
                                        <span>12000</span>
                                    </div>
                                    <div className="price-table-body-row-item price-table-body-row-item-price">
                                        <span>195</span>
                                    </div>
                                    <div className="price-table-body-row-item">
                                        <span>$1476.00</span>
                                    </div>
                                </div>
                                <div className="price-table-body-row">
                                    <div className="price-table-body-row-item">
                                        <span>13000</span>
                                    </div>
                                    <div className="price-table-body-row-item price-table-body-row-item-price">
                                        <span>195</span>
                                    </div>
                                    <div className="price-table-body-row-item">
                                        <span>$1576.00</span>
                                    </div>
                                </div>
                                <div className="price-table-body-row">
                                    <div className="price-table-body-row-item">
                                        <span>14000</span>
                                    </div>
                                    <div className="price-table-body-row-item price-table-body-row-item-price">
                                        <span>195</span>
                                    </div>
                                    <div className="price-table-body-row-item">
                                        <span>$1676.00</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="price-container">
                        <Title title="Full Purchase"/>
                        <div className="price-table">
                            <div className="price-table-header">
                                <div className="price-table-header-item">
                                    <span>Capacity</span>
                                </div>
                                <div className="price-table-header-item">
                                    <span>CPS</span>
                                </div>
                                <div className="price-table-header-item">
                                    <span>FLEX Monthly Fee</span>
                                </div>
                            </div>
                            <div className="price-table-body">
                                <div className="price-table-body-row">
                                    <div className="price-table-body-row-item">
                                        <span>2000</span>
                                    </div>
                                    <div className="price-table-body-row-item price-table-body-row-item-price">
                                        <span>75</span>
                                    </div>
                                    <div className="price-table-body-row-item">
                                        <span>$6,400.00</span>
                                    </div>
                                </div>
                                <div className="price-table-body-row">
                                    <div className="price-table-body-row-item">
                                        <span>3000</span>
                                    </div>
                                    <div className="price-table-body-row-item price-table-body-row-item-price">
                                        <span>90</span>
                                    </div>
                                    <div className="price-table-body-row-item">
                                        <span>$7,100.00</span>
                                    </div>
                                </div>
                                <div className="price-table-body-row">
                                    <div className="price-table-body-row-item">
                                        <span>4000</span>
                                    </div>
                                    <div className="price-table-body-row-item price-table-body-row-item-price">
                                        <span>105</span>
                                    </div>
                                    <div className="price-table-body-row-item">
                                        <span>$7,800.00</span>
                                    </div>
                                </div>
                                <div className="price-table-body-row">
                                    <div className="price-table-body-row-item">
                                        <span>5000</span>
                                    </div>
                                    <div className="price-table-body-row-item price-table-body-row-item-price">
                                        <span>120</span>
                                    </div>
                                    <div className="price-table-body-row-item">
                                        <span>$8,500.00</span>
                                    </div>
                                </div>
                                <div className="price-table-body-row">
                                    <div className="price-table-body-row-item">
                                        <span>6000</span>
                                    </div>
                                    <div className="price-table-body-row-item price-table-body-row-item-price">
                                        <span>135</span>
                                    </div>
                                    <div className="price-table-body-row-item">
                                        <span>$9,200.00</span>
                                    </div>
                                </div>
                                <div className="price-table-body-row">
                                    <div className="price-table-body-row-item">
                                        <span>7000</span>
                                    </div>
                                    <div className="price-table-body-row-item price-table-body-row-item-price">
                                        <span>150</span>
                                    </div>
                                    <div className="price-table-body-row-item">
                                        <span>$9,900.00</span>
                                    </div>
                                </div>
                                <div className="price-table-body-row">
                                    <div className="price-table-body-row-item">
                                        <span>8000</span>
                                    </div>
                                    <div className="price-table-body-row-item price-table-body-row-item-price">
                                        <span>165</span>
                                    </div>
                                    <div className="price-table-body-row-item">
                                        <span>$10,600.00</span>
                                    </div>
                                </div>
                                <div className="price-table-body-row">
                                    <div className="price-table-body-row-item">
                                        <span>9000</span>
                                    </div>
                                    <div className="price-table-body-row-item price-table-body-row-item-price">
                                        <span>180</span>
                                    </div>
                                    <div className="price-table-body-row-item">
                                        <span>$11,300.00</span>
                                    </div>
                                </div>
                                <div className="price-table-body-row">
                                    <div className="price-table-body-row-item">
                                        <span>10000</span>
                                    </div>
                                    <div className="price-table-body-row-item price-table-body-row-item-price">
                                        <span>195</span>
                                    </div>
                                    <div className="price-table-body-row-item">
                                        <span>$12,000.00</span>
                                    </div>
                                </div>
                                <div className="price-table-body-row">
                                    <div className="price-table-body-row-item">
                                        <span>10000</span>
                                    </div>
                                    <div className="price-table-body-row-item price-table-body-row-item-price">
                                        <span>210</span>
                                    </div>
                                    <div className="price-table-body-row-item">
                                        <span>$12,700.00</span>
                                    </div>
                                </div>
                                <div className="price-table-body-row">
                                    <div className="price-table-body-row-item">
                                        <span>11000</span>
                                    </div>
                                    <div className="price-table-body-row-item price-table-body-row-item-price">
                                        <span>235</span>
                                    </div>
                                    <div className="price-table-body-row-item">
                                        <span>$13,400.00</span>
                                    </div>
                                </div>
                                <div className="price-table-body-row">
                                    <div className="price-table-body-row-item">
                                        <span>12000</span>
                                    </div>
                                    <div className="price-table-body-row-item price-table-body-row-item-price">
                                        <span>250</span>
                                    </div>
                                    <div className="price-table-body-row-item">
                                        <span>$14,100.00</span>
                                    </div>
                                </div>
                                <div className="price-table-body-row">
                                    <div className="price-table-body-row-item">
                                        <span>13000</span>
                                    </div>
                                    <div className="price-table-body-row-item price-table-body-row-item-price">
                                        <span>265</span>
                                    </div>
                                    <div className="price-table-body-row-item">
                                        <span>$14,800.00</span>
                                    </div>
                                </div>
                                <div className="price-table-body-row">
                                    <div className="price-table-body-row-item">
                                        <span>14000</span>
                                    </div>
                                    <div className="price-table-body-row-item price-table-body-row-item-price">
                                        <span>280</span>
                                    </div>
                                    <div className="price-table-body-row-item">
                                        <span>$15,500.00</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Container>
        <Footer/>
        </>
    )
}