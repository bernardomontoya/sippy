import React from 'react';
import SVG from './svg.component';
import styled from 'styled-components';
import { ReactComponent as Redundancy } from '../assets/svg/redundancy.svg';
import { ReactComponent as BusinessReporting } from '../assets/svg/business-reporting.svg';
import { ReactComponent as flexiblerouting } from '../assets/svg/flexible-routing.svg';
import { ReactComponent as realtime } from '../assets/svg/real-time.svg';
import { ReactComponent as BillingManagement } from '../assets/svg/billing-management.svg';
import { ReactComponent as systemadministration } from '../assets/svg/system-administration.svg';
import { ReactComponent as xmlrpc } from '../assets/svg/xmlrpc.svg';
import { ReactComponent as SupportandSecurity } from '../assets/svg/support-and-security.svg';
import { ReactComponent as SippyEnvironments } from '../assets/svg/sippy-environments.svg';
import { ReactComponent as callingcardmodule } from '../assets/svg/calling-card-module.svg';
import { ReactComponent as WebCallbackModule } from '../assets/svg/web-call-back.svg';
import { ReactComponent as DIDManagement } from '../assets/svg/did-management-tools.svg';
import { ReactComponent as DONOTCALLLIST } from '../assets/svg/do-not-call-list.svg';
import { ReactComponent as TurkishRegulatoryFeatures } from '../assets/svg/turkish.svg';
import { ReactComponent as ListofSupporteCurrencies } from '../assets/svg/currencies-supported.svg';

const Nav = styled.div`
    width: 100%;
    padding: 0 40px;
    display: flex;
    flex-direction: column;
    align-items: center;
`
const NavContainer = styled.div`
    display: flex;
    flex-direction: row;
`
const NavElementContainer = styled.div`
    width: 100%;
    padding: 20px;
`

export default function FeaturesNav(props) {
    const currentFeature = props.feature;
    return(
        <Nav>
            <NavContainer className="sippy-container">
                <NavElementContainer>
                    <a href={'/features/flexible-routing-tools'} variant="outlined" title="Flexible Routing Tools" size="small">
                        <SVG element={flexiblerouting} width={100} />
                    </a>
                </NavElementContainer>
                <NavElementContainer>
                    <a href={'/features/real-time'} title="Real Time Monitoring" variant="outlined" size="small">
                        <SVG element={realtime} width={100} />
                    </a>
                </NavElementContainer>
                <NavElementContainer>
                    <a href={'/features/business-reporting'} title="Business Reporting" variant="outlined" size="small">
                        <SVG element={BusinessReporting} width={100} />
                    </a>
                </NavElementContainer>
                <NavElementContainer>
                    <a href={'/features/billing-management'} title="Billing Management" variant="outlined" size="small">
                        <SVG element={BillingManagement} width={100} />
                    </a>
                </NavElementContainer>
                <NavElementContainer>
                    <a href={'/features/system-administration'} title="System Administration" variant="outlined" size="small">
                        <SVG element={systemadministration} width={100} />
                    </a> 
                </NavElementContainer>
                <NavElementContainer>
                    <a href={'/features/xml-rpc'} title="XML RPC API" variant="outlined" size="small">
                        <SVG element={xmlrpc} width={100} />
                    </a>
                </NavElementContainer>
                <NavElementContainer>
                    <a href={'/features/support-and-security'} title="Support And Security" variant="outlined" size="small">
                        <SVG element={SupportandSecurity} width={100} />
                    </a>
                </NavElementContainer>
                <NavElementContainer>
                    <a href={'/features/sippy-environments'} title="Sippy Environments" variant="outlined" size="small">
                        <SVG element={SippyEnvironments} width={100} />
                    </a>
                </NavElementContainer>
                <NavElementContainer>
                    <a href={'/features/calling-card-module'} title="Calling Card Module" variant="outlined" size="small">
                        <SVG element={callingcardmodule} width={100} />
                    </a>
                </NavElementContainer>
                <NavElementContainer>
                    <a href={'/features/web-callback-module'} title="Web Callback Module" variant="outlined" size="small">
                        <SVG element={WebCallbackModule} width={100} />
                    </a>
                </NavElementContainer>
                <NavElementContainer>
                    <a href={'/features/did-management'} title="DID Management" variant="outlined" size="small">
                        <SVG element={DIDManagement} width={100} />
                    </a>
                </NavElementContainer>
                <NavElementContainer>
                    <a href={'/features/do-not-call-list'} title="Do not call list" variant="outlined" size="small">
                        <SVG element={DONOTCALLLIST} width={100} />
                    </a>
                </NavElementContainer>
                <NavElementContainer>
                    <a href={'/features/turkish-regulatory-features'} title="Regulatory" variant="outlined" size="small">
                        <SVG element={TurkishRegulatoryFeatures} width={100} />
                    </a>
                </NavElementContainer>
                <NavElementContainer>
                    <a href={'/features/list-of-supported-currencies'} title="List of supported currencies" variant="outlined" size="small">
                        <SVG element={ListofSupporteCurrencies} width={100} />
                    </a>
                </NavElementContainer>
            </NavContainer>
        </Nav>
    )
}