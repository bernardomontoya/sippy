import React from 'react';
import styled from 'styled-components';

const StyledTitle = styled.h1`
    color: #FFF;
    font-size: 16px;
`

export default function NavLink(props) {
    return(
        <StyledTitle>
            {props.title}
        </StyledTitle>
    )
}