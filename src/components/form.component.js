import React from 'react';
import Button from '@material-ui/core/Button';
import styled from 'styled-components';
import Iframe from 'react-iframe'
import { device } from './devices';
import Footer from './footer.component';
const Container = styled.div`
    width: 100%;
    padding: 40px 0 40px;
    display: flex;
    flex-direction: column;
    align-items: center;
    padding-bottom: 80px;
    overflow: auto;
    height: auto;
    & .sippy-container {
        position: relative;
        height: 692px;
        display: flex;
    flex-direction: column;
    align-items: center;
    overflow: auto;
        & iframe  {
            position: absolute;
            border: solid;
            border-color: #ec8d2f;
        }
    }
    & .sippy-about-partners-container {
        margin-bottom: 40px;
    }
    & .sippy-about-partner-row {
        width: 100%;
        display: flex;
        flex-direction: row;
        margin-bottom: 12px;
        &:last-child {
            margin-bottom: 0;
        }
        & .sippy-about-partner {
            background: #FFF;
            border-radius: 5px;
            height: 200px;
            width: 100%;
            display: flex;
            margin-right: 12px;
            &:last-child {
                margin-right: 0;
            }
        }
    }
`
export default function form() {
    return(
        <>
        <Container>
            <div className="sippy-container">
            <iframe height="100%" title="Embedded Wufoo Form" allowtransparency="true" frameborder="0" scrolling="yes" style={{width: "50%"}} src="https://sippysoft.wufoo.com/embed/rb3puzy0uudbat/"> <a href="https://sippysoft.wufoo.com/forms/rb3puzy0uudbat/">Fill out my Wufoo form!</a> </iframe>
            </div>
           
            
        </Container>
        <Footer/>
        </>
    )
}