import React from 'react';
import styled from 'styled-components';
import { device } from './devices';

export default function Paragraph(props) {
    const size = props.size;
    const alignment = props.alignment;
    const color = props.color;
    const StyledParagraph = styled.p`
        font-size: ${size === "small" ? "18px" : size === "medium" ? "20px" : size === "verysmall" ? "14px" : "28px"};
        color: ${ color !== undefined ? color : '#989898' };
        margin-bottom: 30px;
        text-align: ${ alignment !== undefined ? alignment : 'left' };
        width: 100%;
        line-height: 28px;
        &:last-child {
            margin-bottom: 0;
        }
        @media (max-width: ${device.mobileL}) {
            font-size: 20px;
        }
    `
    return(
        <StyledParagraph>{props.text}</StyledParagraph>
    )
}