import React from 'react';
import styled from 'styled-components';
import Button from '@material-ui/core/Button';
import Paragraph from './paragraph.component';
import Title from './title.component';
import { device } from './devices';
import { ReactComponent as MediaGateway } from '../assets/svg/media-gateway.svg';
import { ReactComponent as Redundancy } from '../assets/svg/redundancy.svg';
import { ReactComponent as SoftSwitch } from '../assets/svg/softswitch.svg';
import RedundancyImage from '../assets/images/techr.png';
import SippyImage from './image.component';
import SVG from './svg.component';
import Footer from './footer.component';

const Container = styled.div`
    width: 100%;
    padding: 60px 40px 80px;
    display: flex;
    flex-direction: column;
    align-items: center;
    & .sippy-technical-information-container {
        width: 100%;
        display: flex;
        flex-direction: row;
        & .sippy-technical-information-section {
            width: 100%;
            margin-right: 40px;
            display: flex;
            flex-direction: column;
            align-items: center;
            &:last-child {
                margin-right: 0;
            }
            & a {
                color: #ec8d2f;
                border-color: #ec8d2f;
            }
            & .sippy-technical-information-section-title {
                height: 100px;
            }
        }
        @media (max-width: ${device.mobileL}) {
            flex-direction: column;
            & > div {
                margin-bottom: 60px;
                margin-right: 0 !important;
            }
        }
    }
    & .sippy-technical-information-text-container {
        padding-top: 20px;
    }
`
const CustomImage = styled.img`
    width: 50%;
    padding-bottom: 60px;
`

export default function Products() {
    return(
        <>
        <Container>
            <div className="sippy-container sippy-container-column">
                <div className="sippy-technical-information-container mb5">
                    <div className="sippy-technical-information-section">
                        <div className="sippy-technical-information-section-title">
                            <Title title="Sippy Media Gateway"/>
                        </div>
                        <SVG element={MediaGateway} width={50} />
                        <Button href="products/media-gateway" variant="outlined" size="large">Read more</Button>
                    </div>
                    <div className="sippy-technical-information-section">
                        <div className="sippy-technical-information-section-title">
                            <Title title="Softswitch"/>
                        </div>
                        <SVG element={SoftSwitch} width={50} />
                        <Button href="products/softswitch" variant="outlined" size="large">Read more</Button>
                    </div>
                    <div className="sippy-technical-information-section">
                        <div className="sippy-technical-information-section-title">
                            <Title title="Redundancy"/>
                        </div>
                        <SVG element={Redundancy} width={50} />
                        <Button href="products/redundancy" variant="outlined" size="large">Read more</Button>
                    </div>
                </div>
                <div className="sippy-technical-information-text-container"> 
                <Title title="Class 4 Wholesale and Class 5 Retail"/>
                <Paragraph size="medium" text="Used by fixed-line and mobile carriers, MVNOs, Wholesalers, Call Center providers, and retail voice providers worldwide. With additional features such as LRN dip, IPSEC interconnects capabilities, Do Not Call List management, DID Management, and much more."/>
                <Paragraph size="medium" text="Softswitch solutions designed to be as flexible as your business. WIth built-in web portals for Administration, Customers, Resellers, and accounts - OR - build your own portals and integrate with your existing systems using the Sippy APIs.  Scale your voice business with confidence."/>
                <Paragraph size="medium" text="Sippy provides multiple pricing options on one of the most secure Softswitch solutions in its class!!!"/>
                </div>
            </div>
        </Container>
        <Footer/>
        </>
    )
}