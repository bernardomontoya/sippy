import React from 'react';
import styled from 'styled-components';
import Button from '@material-ui/core/Button';

const StyledButton = styled(Button)`
    color: #ec8d2f !important;
    border-color: #ec8d2f !important;
    filter: drop-shadow(0 0 6px rgb(226,113,0));
`

export default function SippyButton(props) {
    const label = props.label;
    const size = props.size;
    const hiper = props.hiper;
    return(
        <a href={hiper} target="_blank"><StyledButton variant="outlined" size={size} >{label}</StyledButton></a>
        
    )
}