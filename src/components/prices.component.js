import React, { useState } from "react";
import { useMediaQuery } from "react-responsive";
import Popover from "@material-ui/core/Popover";
import styled from "styled-components";
import Title from "./title.component";
import Paragraph from "./paragraph.component";
import Footer from "./footer.component";
import Flexible from "../assets/images/flexible.png";
import Button from "@material-ui/core/Button";
import Select from "@material-ui/core/Select";
import FormControl from "@material-ui/core/FormControl";
import MenuItem from "@material-ui/core/MenuItem";
import Calculator from "../containers/calculator.container";
import { ReactComponent as ComparePricesSVG } from "../assets/svg/compare-prices.svg";
import { ReactComponent as HostingShared } from "../assets/svg/hosting-shared.svg";
import { ReactComponent as HostingSelf } from "../assets/svg/hosting-self.svg";
import { ReactComponent as HostingDedicated } from "../assets/svg/hosting-dedicated.svg";
import Tooltip from "@material-ui/core/Tooltip";
import { withStyles } from "@material-ui/core/styles";

const CustomTooltip = withStyles({
  tooltip: {
    fontSize: "18px",
  },
})(Tooltip);

// styles
const FlexibleImage = styled.img`
  width: 20%;
`;
const QuoteButton = styled(Button)`
  color: #ec8d2f !important;
  border-color: #ec8d2f !important;
  filter: drop-shadow(0 0 6px rgb(226, 113, 0));
`;
const ComparePrices = styled(ComparePricesSVG)`
  width: 200px;
  height: auto;
`;
const HostingSVG = styled.div`
  width: 100%;
  & svg {
    width: 100%;
    height: auto;
  }
`;

export default function Prices() {
  const [iconMessage, setIconMessage] = useState("");
  const [anchorEl, setAnchorEl] = React.useState(null);
  const isTabletOrMobile = useMediaQuery({ maxWidth: 1224 });
  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;
  const handleClick = (event, message) => {
    setAnchorEl(event.currentTarget);
    setIconMessage(message);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  return (
    <>
      <div className="ph3 ph6-ns pv5 pv6-ns">
        <div className="flex flex-column items-center">
          <Title title="Sippy is flexible and will work for you" />
          <FlexibleImage src={Flexible} />
          <p className="white tc f3 mb5">
            This price calculator will let you determine which options work best
            for you based on multiple offerings
          </p>
          <div className="flex flex-column flex-row-ns w-100 mb5">
            <div className="flex flex-column w-100 w-30-ns justify-center items-center mb5 mb0-ns">
              <a href="/prices/compare" className="white flex flex-column">
                <ComparePrices className="mb4" />
                <span>Click here to se all prices</span>
              </a>
            </div>
            <Calculator />
            <div className="flex flex-column w-100 w-30-ns items-center justify-center mt5 mt0-ns">
              <div className="w-100 flex flex-column flex-row-ns items-center">
                <div className="flex flex-column w-50 w-100-ns mb4 mb0-ns">
                  <HostingSVG
                    className="pointer"
                    onClick={(event) => {
                      handleClick(
                        event,
                        "Sippy’s Hosted solutions are deployed on enterprise-grade bare­ metal servers within IBM Cloud data Centers in the US and Europe."
                      );
                    }}
                  >
                    <HostingShared />
                  </HostingSVG>
                </div>
                <div className="flex flex-column w-50 w-100-ns mb4 mb0-ns">
                  <HostingSVG
                    className="pointer"
                    onClick={(event) => {
                      handleClick(
                        event,
                        "Sippy can manage your server along with its maintenance and guarantee that the enterprise-grade, bare-metal server you pay for monthly serves only your needs."
                      );
                    }}
                  >
                    <HostingDedicated />
                  </HostingSVG>
                </div>
                <div className="flex flex-column w-50 w-100-ns">
                  <HostingSVG
                    className="pointer"
                    onClick={(event) => {
                      handleClick(
                        event,
                        "Sippy can provision a softswitch you can host on your own infrastructure."
                      );
                    }}
                  >
                    <HostingSelf />
                  </HostingSVG>
                </div>
              </div>
              {isTabletOrMobile ? (
                <Popover
                  id={id}
                  open={open}
                  anchorEl={anchorEl}
                  onClose={handleClose}
                  anchorOrigin={{
                    vertical: "bottom",
                    horizontal: "center",
                  }}
                  transformOrigin={{
                    vertical: "top",
                    horizontal: "center",
                  }}
                >
                  <p className="white">{iconMessage}</p>
                </Popover>
              ) : (
                <div class="w-100 pa4 h-100-l f5-ns">
                  <p className="white">{iconMessage}</p>
                </div>
              )}
            </div>
          </div>
          <p className="white tc f3 mb5">
            Call or email us to discuss the specifics of the purchase option you
            select
          </p>
          <QuoteButton href="#" variant="outlined" size="large">
            Request Quote
          </QuoteButton>
        </div>
      </div>
      <Footer />
    </>
  );
}
