import React from 'react';
import styled from 'styled-components';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import ReactPlayer from 'react-player'
import VideoCover from '../assets/images/video_cover.png'
import { device } from './devices';

const SippyVideo = styled.div`
    padding: 80px 0;
    & .sippy-video {
        width: 620px;
        height: 360px;
        background-color: #ccc !important;
        @media (max-width: ${device.mobileL}) {
            width: 100%;
        }
        & .sippy-video-overlay {
            width: 100%;
            height: 100%;
            display: flex;
            align-items: center;
            justify-content: center;
            & button {
                color: #ec8d2f;
                & svg {
                    color: #ec8d2f;
                    width: 60px;
                    height: 60px;
                }
            }
        }
    }
`

export default function Video(props) {
    const hasButton = props.hasButton;
    const buttonLabel = props.buttonLabel;
    const video = props.src;
    console.log(video);
    return (
        <SippyVideo>
            <ReactPlayer url={video} controls={true} light={VideoCover} />
            {
                hasButton
                ?
                <Button>{buttonLabel}</Button>
                :
                null
            }
        </SippyVideo>
    )
}