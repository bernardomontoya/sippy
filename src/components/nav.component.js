import React from "react";
import styled from "styled-components";
import { NavLink } from "react-router-dom";
import Logo from "../assets/images/logo_footer.png";
import LogoBall from "../assets/images/logo_ball.png";
import Paragraph from "./paragraph.component";
import { ReactComponent as HomeSVG } from "../assets/svg/home.svg";
import { ReactComponent as AboutSVG } from "../assets/svg/about.svg";
import { ReactComponent as ProductsSVG } from "../assets/svg/products.svg";
import { ReactComponent as FeaturesSVG } from "../assets/svg/features.svg";
import { ReactComponent as TechnicalSVG } from "../assets/svg/technical.svg";
import { ReactComponent as PricesSVG } from "../assets/svg/prices.svg";
import { ReactComponent as ContactSVG } from "../assets/svg/contact.svg";
import { ReactComponent as FraudSVG } from "../assets/svg/fraud.svg";
import { device } from "./devices";

const NavContainer = styled.div`
  width: 100%;
  height: 100px;
  background: #333333;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  transition: 0.2s all ease;
  & .sippy-nav-logo {
    width: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    & img {
      width: 50px;
      &.sippy-nav-full-logo {
        display: none;
      }
    }
  }
  & .sippy-nav-links {
    width: 100%;
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    & a {
      margin-right: 40px;
      text-align: center;
      opacity: 0.4;
      &:last-child {
        margin-right: 0;
      }
      & svg {
        width: 35px;
        height: 35px;
        fill: #ec8d2f;
        transition: 0.2s all ease;
        & circle {
          stroke: #ec8d2f;
        }
      }
      & .sippy-nav-text-container {
        width: 80px;
        display: none;
        color: #fff;
        font-size: 18px;
        margin-top: 14px;
        margin-top: 22px;
        transition: 0.2s all ease;
      }
    }
    & a.active {
      opacity: 1;
      & svg {
        -webkit-filter: drop-shadow(0 0 6px rgb(226, 113, 0));
        filter: drop-shadow(0 0 6px rgb(226, 113, 0));
        width: 45px;
        height: 45px;
      }
    }
    & .sippy-nav-logo-image {
      width: 36px;
    }
  }
  & .sippy-nav-text {
    display: none;
    padding: 0 40px;
    pointer-events: none;
  }
  & .sippy-hashtag  {
    opacity: 0;
    transition: opacity 0.2s ease;
    position: absolute;
    top: 40px;
    right: 40px;
    color: #f7921f;
    font-size: 18px;
    font-weight: 500;
    pointer-events: none;
  }
  &.home-active {
    height: 100%;
    & .sippy-nav-ball-logo {
      display: none !important;
    }
    & .sippy-nav-full-logo {
      display: flex !important;
      width: 220px;
      margin-bottom: 80px;
    }
    & .sippy-nav-links {
      margin-bottom: 80px;
      align-items: flex-start;
      & a {
        margin-right: 100px;
        opacity: 1;
        & svg {
          width: 62px;
          height: 62px;
        }
        & .sippy-nav-text-container {
          display: flex !important;
          align-items: center;
          justify-content: center;
          text-align: center;
        }
      }
    }
    & .sippy-nav-text {
      display: flex;
    }
    & .sippy-nav-home {
      display: none;
    }
    & .sippy-hashtag  {
      opacity: 1;
    }
  }
  @media (max-width: ${device.mobileL}) {
    .sippy-nav-links {
      padding: 0 40px;
      & a {
        margin-right: 22px;
        & svg {
          width: 28px;
          height: 28px;
        }
      }
    }
    &.home-active {
      & .sippy-nav-full-logo {
        margin-bottom: 22px;
        width: 140px;
      }
      .sippy-nav-links {
        flex-direction: column;
        & a {
          margin-right: 0;
          margin-bottom: 30px;
          &:last-child {
            margin-bottom: 0;
          }
          & svg {
            width: 28px;
            height: 28px;
          }
          & .sippy-nav-text-container {
            width: 100%;
            margin-top: 10px;
            font-size: 14px;
          }
        }
      }
    }
  }
`;

export default function Nav(props) {
  return (
    <NavContainer className="sippy-nav">
      <div className="sippy-nav-logo">
        <NavLink to="/">
          <img
            className="sippy-nav-full-logo"
            src={Logo}
            alt="sippy software"
          />
        </NavLink>
      </div>
      <div className="sippy-nav-links">
        <NavLink to="/" exact className="sippy-nav-home">
          <img
            className="sippy-nav-logo-image"
            src={LogoBall}
            alt="sippy software"
          />
          <div className="sippy-nav-text-container">
            <span>Home</span>
          </div>
        </NavLink>
        <NavLink to="/about">
          <AboutSVG />
          <div className="sippy-nav-text-container">
            <span>About</span>
          </div>
        </NavLink>
        <NavLink to="/products">
          <ProductsSVG />
          <div className="sippy-nav-text-container">
            <span>Products</span>
          </div>
        </NavLink>
        <NavLink to="/features">
          <FeaturesSVG />
          <div className="sippy-nav-text-container">
            <span>Features</span>
          </div>
        </NavLink>
        <NavLink to="/technical-information">
          <TechnicalSVG />
          <div className="sippy-nav-text-container">
            <span>Tech</span>
          </div>
        </NavLink>
        <NavLink to="/prices">
          <PricesSVG />
          <div className="sippy-nav-text-container">
            <span>Pricing</span>
          </div>
        </NavLink>
        <NavLink to="/fraud">
          <FraudSVG />
          <div className="sippy-nav-text-container">
            <span>Anti Fraud</span>
          </div>
        </NavLink>
        <NavLink to="/contact-us">
          <ContactSVG />
          <div className="sippy-nav-text-container">
            <span>Contact</span>
          </div>
        </NavLink>
      </div>
      <div className="sippy-nav-text">
        <Paragraph
          size="medium"
          alignment="center"
          color="#f7921f"
          text="The most secure, scalable, flexible, and economic voice technology solutions delivered to global Mobile, MVNO, and Fixed Line Operators, Wholesalers, Call Centers, and Retail/Enterprise SIP solution providers."
        ></Paragraph>
      </div>
      <span className="sippy-hashtag">#connectingcarriers</span>
    </NavContainer>
  );
}
