import React from 'react';
import styled from 'styled-components';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Video from './video.component';
import { device } from './devices';

export default function IntroSection(props) {
    const type = props.type;
    const icons = props.icons;
    const video = props.video;
    const hasButton = props.hasButton;
    const buttonLabel = props.buttonLabel;
    return (
        <div>
            {
                type === "icon"
                ?
                    null
                :
                <Video src={video} hasButton={hasButton} buttonLabel={buttonLabel} />
            }
        </div>
    )
}