import React from 'react';
import styled from 'styled-components';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import { device } from './devices';

export default function SippyImage(props) {
    const src = props.src;
    const alt = props.alt;
    const size = props.size;
    const width = props.width;
    const ImageContainer = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    & img {
        width: ${width !== undefined ? width : 60}%;
    }
`
    return (
        <ImageContainer>
            <img src={src} alt={alt}/>
        </ImageContainer>
    )
}