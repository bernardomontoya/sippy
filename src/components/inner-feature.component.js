import React from 'react';
import styled from 'styled-components';
import Title from './title.component';
import Paragraph from './paragraph.component';
import SVG from './svg.component';
import Footer from './footer.component';
import FeaturesNav from './features-navigation.component';
import { ReactComponent as MediaGateway } from '../assets/svg/media-gateway.svg';
import { ReactComponent as BusinessReporting } from '../assets/svg/business-reporting.svg';
import { ReactComponent as flexiblerouting } from '../assets/svg/flexible-routing.svg';
import { ReactComponent as realtime } from '../assets/svg/real-time.svg';
import { ReactComponent as BillingManagement } from '../assets/svg/billing-management.svg';
import { ReactComponent as systemadministration } from '../assets/svg/system-administration.svg';
import { ReactComponent as xmlrpc } from '../assets/svg/xmlrpc.svg';
import { ReactComponent as SupportandSecurity } from '../assets/svg/support-and-security.svg';
import { ReactComponent as SippyEnvironments } from '../assets/svg/sippy-environments.svg';
import { ReactComponent as callingcardmodule } from '../assets/svg/calling-card-module.svg';
import { ReactComponent as WebCallbackModule } from '../assets/svg/web-call-back.svg';
import { ReactComponent as DIDManagement } from '../assets/svg/did-management-tools.svg';
import { ReactComponent as DONOTCALLLIST } from '../assets/svg/do-not-call-list.svg';
import { ReactComponent as TurkishRegulatoryFeatures } from '../assets/svg/turkish.svg';
import { ReactComponent as ListofSupporteCurrencies } from '../assets/svg/currencies-supported.svg';
import Partner4 from '../assets/images/partner4.JPG';
import SippyImage from './image.component';

const Container = styled.div`
    width: 100%;
    padding: 80px 40px 0;
    display: flex;
    flex-direction: column;
    align-items: center;
    margin-bottom: 80px;
    & .sippy-subtitle {
        font-size: 22px;
        color: #ec8d2f;
        margin-bottom: 32px;
    }
    & .sippy-hr-color {
        border: 1px solid #f8941e;
        width: 100%;
    }
    & .sippy-list {
        color: #afafaf;
        margin: 0;
        margin-bottom: 60px;
        font-size: 20px;
        width: 100%;
        & li {
            margin-bottom: 8px;
            &:last-child {
                margin-bottom: 0;
            }
        }
    }
    iframe {
        background: white;
        & div#tableWrapper {
            border: solid;
            max-height: none !important;
        }
    }
    a:link {
        color: #ec8d2f;
        background-color: transparent;
        text-decoration: none;
      }
      
      a:visited {
        color: #ec8d2f;
        background-color: transparent;
        text-decoration: none;
      }
      
      a:hover {
        color: white;
        background-color: transparent;
        text-decoration: underline;
      }
      
      a:active {
        color: yellow;
        background-color: transparent;
        text-decoration: underline;
      }
    
`

function ShowFeature(feature) {
    if (feature === 'flexible-routing-tools') {
        return(
            <>
            <FeaturesNav feature={feature}/>
            <Container>
                <div className="sippy-container sippy-container-column">
                <Title title="Flexible Routing Tools"/>
                <SVG element={flexiblerouting} width={25} />
                <Paragraph size="medium" text='Based on Sippy B2BUA, Sippy Software has some of the most dynamic and flexible routing options available.  Routing options can be imposed individually, or nested to provide even more routing options.   The Sippy "Test Dialplan" tool provides the ability to verify Dialplans work according to the routing policies.   Quality routing provides the ability to configure your Sippy Softswitch to automatically remove poorly performing routes, and automatically move calls to predefined alternative routes.  When switching to alternative routes, or where a call to route to a premium route (with little or no margin), the Sippy "Loss Protection" feature will detect this error, and prevent the calls from completing!'/>
                <span className="sippy-subtitle">Key Advantages:</span>
                <ul className="sippy-list">
                    <li>
                        <span>Unlimited Supplier (Vendor) Connections</span>
                    </li>
                    <li>
                        <span>Adaptive Routing based on ASR/ACD (Flexible Crankback management)</span>
                        <ul>
                            <li>Least Cost Routing (LCR)</li>
                            <li>Percentage based routing</li>
                            <li>Forced/preference routing</li>
                            <li>Prefix length routing</li>
                        </ul>
                    </li>
                    <li>
                        <span>ASR / ACD Quality monitoring</span>
                    </li>
                    <li>
                        <span>Profit / Loss Protection (Margin based Routing)</span>
                    </li>
                    <li>
                        <span>Jurisdictional Routing</span>
                        <ul>
                            <li>Inter/Intra State</li>
                        </ul>
                    </li>
                    <li>
                        <span>On-Net Routing and Rating</span>
                    </li>
                    <li>
                        <span>LRN / Number Portability Lookup</span>
                    </li>
                    <li>
                        <span>T.38 Fax Support</span>
                    </li>
                    <li>
                        <span>DNCL Supported - Black List, Block List, Do-not call list.</span>
                    </li>
                    <li>
                        <span>Test Dialplan tool - Call routing simulator / debugger</span>
                        <ul>
                            <li>Internal Routing and Rating config test tool</li>
                        </ul>
                    </li>
                    <li>
                        <span>Codec Agnostic</span>
                    </li>
                    <li>
                        <span>Protocol support</span>
                        <ul>
                            <li>SIP</li>
                            <li>GSM, G.711a, G.711u, G.722, G.723, G.728, G.729 Pass-through</li>
                        </ul>
                    </li>
                </ul>
                </div>
            </Container>
            <Footer/>
            </>
        )
    }
    else if (feature === 'real-time') {
        return(
            <>
            <FeaturesNav feature={feature}/>
            <Container>
                <div className="sippy-container sippy-container-column">
                <Title title="Real-Time Monitoring"/>
                <SVG element={realtime} width={25} />
                <Paragraph size="medium" text="The Voice telecom business moves fast, it is important to be able to make real-time business decisions based on hard, accurate information.  To meet business information needs, Sippy provides Real-Time monitoring with the ability to check data on a moment by month basis - extend your view on the data to a time period that suits your needs.  Real-time monitoring includes: "/>
                <ul className="sippy-list">
                    <li>
                        <span>'Real-time' time series graphs</span>
                        <ul>
                            <li>Call Processing time - Time to Vendor, SIP, LRN, AA.</li>
                            <li>Calls Per Second - Ingress cps, Egress cps, Authorized, Routed,Connected.</li>
                            <li>In Progress Calls / Concurrent Calls (Channels) - Total, Connected</li>
                        </ul>
                    </li>
                    <li>
                        <span>System-Wide ASR / ACD - ACD, ASR all in, ASR all out, ASR authenticated.</span>
                        <ul>
                            <li>Bandwidth In Use</li>
                            <li>Throughput</li>
                        </ul>
                    </li>
                    <li>
                        <span>Active Calls Monitoring</span>
                    </li>
                    <li>
                        <span>Real-time Credit/Balance reconciliation</span>
                    </li>
                    <li>
                        <span>Real-time call rating and billing</span>
                    </li>
                    <li>
                        <span>Network packet capture abilities - ability to capture network traffic for off-line analysis in wireshark or other industry standard tools.</span>
                    </li>
                </ul>
                </div>
            </Container>
            <Footer/>
            </>
        )
    }
    else if (feature === 'business-reporting') {
        return(
            <>
            <FeaturesNav feature={feature}/>
            <Container>
                <div className="sippy-container sippy-container-column">
                <Title title="Business reporting"/>
                <SVG element={BusinessReporting} width={25} />
                <Paragraph size="medium" text="The Voice telecom business moves fast, it is important to be able to make real-time business decisions based on hard, accurate information.  To meet business information needs, Sippy provides carriers with a series of business and commercial reports that enable business owners, sales teams, and network operations teams to make fast, accurate business decisions based on real, accurate data."/>
                <ul className="sippy-list">
                    <li>
                        <span>Customer and Vendor/Supplier CDR’s (Destination, caller etc.) that can be filtered by;</span>
                        <ul>
                            <li>All or individual Customer Vendor/Supplier CDR’s</li>
                            <li>By Date and/or Time Range</li>
                            <li>Call Completion Status (easy to locate specific failed calls)</li>
                        </ul>
                    </li>
                    <li>
                        <span>Commercial Reporting including;</span>
                        <ul>
                            <li>Profit/Loss Reports</li>
                            <li>Flexible Summary reports</li>
                            <li>Sales Reports</li>
                            <li>Payments Reports</li>
                        </ul>
                    </li>
                    <li>
                        <span>Quality Reporting including;</span>
                        <ul>
                            <li>Flexible ASR/ACD Reporting</li>
                        </ul>
                    </li>
                    <li>
                        <span>Download/Email tools</span>
                    </li>
                    <li>
                        <span>Data export</span>
                        <ul>
                            <li>Account Export</li>
                            <li>Tariffs / Rate Deck</li>
                            <li>Route / Code Deck</li>
                            <li>Customer CDRs</li>
                            <li>Supplier CDRs</li>
                        </ul>
                    </li>
                    <li>
                        <span>PDF Invoice export</span>
                    </li>
                </ul>
                </div>
            </Container>
            <Footer/>
            </>
        )
    }
    else if (feature === 'billing-management') {
        return(
            <>
            <FeaturesNav feature={feature}/>
            <Container>
                <div className="sippy-container sippy-container-column">
                <Title title="Billing Management"/>
                <SVG element={BillingManagement} width={25} />
                <Paragraph size="medium" text="The Sippy Billing engine is heavily relied upon not just for voice-related services, but as a unified billing solution for multiple business services (such as SMS and Triple Play solutions like IPTV, video-on-demand and data usage."/>
                <Paragraph size="medium" text="The Sippy billing engine provides a flexible and dynamic suite of tools to initiate new services, provide comprehensive pricing solutions for both pre-paid, post-paid clients, wholesale, carrier, and call center solutions providers.   In addition, the Sippy customer Invoice Template tool allows clients to build unique, custom invoice templates to match the business branding.  Options include:"/>
                <ul className="sippy-list">
                    <li>
                        <span>'Real-time' billing</span>
                    </li>
                    <li>
                        <span>Flexible Weekly, Fortnightly, and Monthly Billing Cycles</span>
                    </li>
                    <li>
                        <span>Configurable billing pattern for call rating (Example: 30:6, 1:1 for per-second billing)</span>
                    </li>
                    <li>
                        <span>Pre and Postpaid Service Plans</span>
                    </li>
                    <li>
                        <span>DID/CLD accessibility surcharge support</span>
                    </li>
                    <li>
                        <span>Configurable Service Plan fixed fees (Example: Monthly Plan fee)</span>
                    </li>
                    <li>
                        <span>Billing failure action management</span>
                    </li>
                    <li>
                        <span>Destination based Minute Plan support</span>
                    </li>
                    <li>
                        <span>Configurable rounding method (round up, round) for Customer and Vendor CDRs</span>
                    </li>
                    <li>
                        <span>Improved Invoicing Generation</span>
                    </li>
                    <li>
                        <span>Payment processor integration for automatic client payments on low balance via Credit Card or Paypal</span>
                    </li>
                    <li>
                        <span>Low Balance notifications and Automatic Payment features</span>
                    </li>
                    <li>
                        <span>Currency support</span>
                    </li>
                    <li>
                        <span>162 default currencies</span>
                    </li>
                    <li>
                        <span>Manual or automatic exchange rate</span>
                    </li>
                    <li>
                        <span>Time zones can be configured on individual Vendor or Client basis</span>
                    </li>
                    <li>
                        <span>External Balance Daemon for Centralized Balance Management</span>
                    </li>
                </ul>
                </div>
            </Container>
            <Footer/>
            </>
        )
    }
    else if (feature === 'system-administration') {
        return(
            <>
            <FeaturesNav feature={feature}/>
            <Container>
                <div className="sippy-container sippy-container-column">
                <Title title="System Administration"/>
                <SVG element={systemadministration} width={25} />
                <Paragraph size="medium" text="Sippy provides your NOC (Network Operations Centre) with the tools to install, support, and maintain the physical servers where our Sippy Softswitch Licenses reside.  These tools include Web-portal and email access to the Sippy technical support team who can provide round-the-clock support, assistance, and guidance on the optimum use of your Sippy Softswitch. Virtually every aspect of your Sippy Softswitch is designed to provide you with complete administration and configuration of the features and as such there are too many options to list in one place.  Some of the core administration features that users request, include:"/>
                <ul className="sippy-list">
                    <li>
                        <span>Network Activity Recorder for analysis and debugging purposes</span>
                    </li>
                    <li>
                        <span>Re-rate CDR tool</span>
                    </li>
                    <li>
                        <span>Web based Ping/Traceroute analysis tool</span>
                    </li>
                    <li>
                        <span>Destination/Prefix management</span>
                    </li>
                    <li>
                        <span>IP Firewall Management for SIP, Web, SSH, and Database access</span>
                    </li>
                    <li>
                        <span>Access Controls for Web users</span>
                    </li>
                    <li>
                        <span>Complete control Web Interface - All switch management features are available from your user interface</span>
                    </li>
                </ul>
                </div>
            </Container>
            <Footer/>
            </>
        )
    }
    else if (feature === 'xml-rpc') {
        return(
            <>
            <FeaturesNav feature={feature}/>
            <Container>
                <div className="sippy-container sippy-container-column">
                <Title title="XML-RPC API Control"/>
                <SVG element={xmlrpc} width={25} />
                <Paragraph size="medium" text='Sippy is provided as an "All-in-One" solution through the WEB Interface.  However, almost all core elements of the Sippy Softswitch are controlled by the powerful XML-RPC APIs, enabling clients to integrate existing in-house management solutions, third-party technologies (such as SMS providers), and networking between different Softswitch and SBC solutions. '/>
                <Paragraph size="medium" text="In short, the Sippy XML-RPC APIs not only offer an advanced method for system administration, they provide clients with an extremely powerful platform for developing extensive IP based telecommunications solutions."/>
                <ul className="sippy-list">
                    <li>
                        <span>Use Sippy as the foundation to your completely flexible VOIP business. Develop Custom solutions and integrations on top of Sippy.</span>
                    </li>
                    <li>
                        <span>Language agnostic - Developers can integrate using their language of choice, python, java, PHP, ruby etc.</span>
                    </li>
                    <li>
                        <span>pload your Rates and Routes through the Binary Upload</span>
                    </li>
                    <li>
                        <span>Manipulate Accounts, Customers and Vendors</span>
                    </li>
                    <li>
                        <span>Manage Environments</span>
                    </li>
                    <li>
                        <span>Access the External Balance Daemon for centralized balance tracking</span>
                    </li>
                    <li>
                        <span>Full documentation available for all our XML-RPC API methods on our support forums located <a href="https://support.sippysoft.com/support/solutions/107132">here</a></span>
                    </li>
                </ul>
                </div>
            </Container>
            <Footer/>
            </>
        )
    }
    else if (feature === 'support-and-security') {
        return(
            <>
            <FeaturesNav feature={feature}/>
            <Container>
                <div className="sippy-container sippy-container-column">
                <Title title="Support and Security"/>
                <SVG element={SupportandSecurity} width={25} />
                <Paragraph size="medium" text="Sippy Software delivers robust, secure Softswitch and SBC solutions, with high availability through hot standby options custom designed to fit the customer's network requirements.   Security is provided through tight internal corporate security policies, continuous OS security patching, and frequent security and management updates to the Sippy Softswitch Code.  Our commitment to security is passed directly to our clients."/>
                <Paragraph size="medium" text="In addition to security patches and updates, standard Sippy support includes security updates, maintenance, and upgrades combined with email and web-portal support from the Sippy 24 x 7 Live Support Centre with staff responding to tickets in Eastern Europe, North America, and South East Asia."/>
                <ul className="sippy-list">
                    <li>
                        <span>Built-in high-performance IP firewall</span>
                    </li>
                    <li>
                        <span>Password security controls</span>
                        <ul>
                            <li>7+ digit/number/symbol length password prompting</li>
                            <li>Manageable password policies</li>
                        </ul>
                    </li>
                    <li>
                        <span>SSH Blacklisting</span>
                    </li>
                    <li>
                        <span>User Audit Logging</span>
                    </li>
                    <li>
                        <span>Installation and Provisioning support</span>
                    </li>
                    <li>
                        <span>Interactive training sessions for all new customers using screen sharing</span>
                    </li>
                    <li>
                        <span>Remote 24x7 monitoring</span>
                    </li>
                    <li>
                        <span>24x7 Rapid response ticketing portal</span>
                    </li>
                    <li>
                        <span>Ticket escalation</span>
                    </li>
                    <li>
                        <span>Urgent direct line Support extension</span>
                    </li>
                    <li>
                        <span>Entitled to all maintenance releases and major version upgrades</span>
                    </li>
                    <li>
                        <span>The in-house Development team for product refinement and extra development</span>
                    </li>
                    <li>
                        <span>Free SSL Certificates through Let's Encrypt</span>
                    </li>
                    <li>
                        <span>Remote Support through Secured SSH access.</span>
                    </li>
                </ul>
                </div>
            </Container>
            <Footer/>
            </>
        )
    }
    else if (feature === 'sippy-environments') {
        return(
            <>
            <FeaturesNav feature={feature}/>
            <Container>
                <div className="sippy-container sippy-container-column">
                <Title title="Sippy Environments"/>
                <SVG element={SippyEnvironments} width={25} />
                <Paragraph size="medium" text="For clients that purchase or rent (Sippy FLEX) their own Sippy Softswitch License, there is the option to create additional virtual Sippy Softswitch Environments (or Partitions).  The main purpose of virtual Sippy Environments is to create an isolated / Self-managed (either your own use or to rent to other route providers) Softswitch that is independent of the main Softswitch and any other Environments.   The Virtual Environment has its own administration, authentication, routing, rating, and billing regardless of whatever else is configured on other Environments."/>
                <Paragraph size="medium" text="Virtual Environments are often used to separate production traffic from trial routes and traffic, to seperate retail traffic from wholesale routes, or to create mini-self contained Softswitch instances to provide to customers needing a Softswitch solution."/>
                <ul className="sippy-list">
                    <li>
                        <span>White-label Reseller portal</span>
                    </li>
                    <li>
                        <span>Capacity Partitioning</span>
                    </li>
                    <li>
                        <span>IP and Port management</span>
                    </li>
                    <li>
                        <span>Expiration date supported</span>
                    </li>
                    <li>
                        <span>Flexible Performance and Capacity management</span>
                    </li>
                    <li>
                        <span>Web Interface accessibility management</span>
                    </li>
                    <li>
                        <span>Available for Dedicated, FLEX and Perpetual purchase licenses</span>
                    </li>
                </ul>
                </div>
            </Container>
            <Footer/>
            </>
        )
    }
    else if (feature === 'calling-card-module') {
        return(
            <>
            <FeaturesNav feature={feature}/>
            <Container>
                <div className="sippy-container sippy-container-column">
                <Title title="Calling Card Module"/>
                <SVG element={callingcardmodule} width={25} />
                <Paragraph size="medium" text="The Sippy Calling Card module provides a multilingual IVR architecture, with real-time call authorization and charging, dynamic call routing, and advanced call control.  Through the Sippy administrative portal, it is easy to configure access numbers, create IVR applications, add tariffs and/or prepaid products, set up resellers, distributors, generate batches of cards (PINs), top-up vouchers, and perform monitoring & troubleshooting."/>
                <Paragraph size="medium" text="Sippy also provides a dynamic rating system, providing the ability to add special fees (such as Ghost and shadow Billing), announcements for call duration and/or call cost."/>
                <Paragraph size="medium" text="In addition to the account and reseller, web-portals allow users to re-review call history data, perform top-up and much more, Sippy provides a complete set of APIs allowing Sippy Clients to create customer web-portal and to  integrate other solutions (such as integrating with reseller POS (Point of Sale)."/>
                <ul className="sippy-list">
                    <li>
                        <span>Web based configuration</span>
                    </li>
                    <li>
                        <span>Comprehensive IVR’s</span>
                    </li>
                    <li>
                        <span>Supported languages</span>
                        <ul>
                            <li>Arabic, Armenia, Chinese, English, French, German, Japanese, Russian, Thai, Turkish, Vietnamese</li>
                        </ul>
                    </li>
                    <li>
                        <span>Recharge Vouchers</span>
                        <ul>
                            <li>Voucher usage Reporting</li>
                            <li>Credit/Voucher expiration</li>
                            <li>Unlimited Bulk generation</li>
                        </ul>
                    </li>
                    <li>
                        <span>PIN management (Account/Card management)</span>
                        <ul>
                            <li>Flexible PIN/Account definition</li>
                            <li>Bulk generation</li>
                            <li>Personal portal</li>
                        </ul>
                    </li>
                    <li>
                        <span>Pin or Pinless Dialing</span>
                    </li>
                    <li>
                        <span>Voicemail</span>
                    </li>
                    <li>
                        <span>Hotkeys</span>
                    </li>
                    <li>
                        <span>Trusted Number Authentication</span>
                    </li>
                    <li>
                        <span>Call Forwarding</span>
                    </li>
                    <li>
                        <span>API Accessibility</span>
                    </li>
                    <li>
                        <span>Account manipulation</span>
                    </li>
                    <li>
                        <span>Payments, credits, debits</span>
                    </li>
                    <li>
                        <span>Card Lifetime supported</span>
                    </li>
                    <li>
                        <span>Invoicing</span>
                    </li>
                    <li>
                        <span>Charges</span>
                    </li>
                    <li>
                        <span>Prefix based Minute plans</span>
                    </li>
                    <li>
                        <span>Mystery minutes</span>
                    </li>
                    <li>
                        <span>Post call surcharges</span>
                    </li>
                    <li>
                        <span>DID management</span>
                    </li>
                </ul>
                </div>
            </Container>
            <Footer/>
            </>
        )
    }
    else if (feature === 'web-callback-module') {
        return(
            <>
            <FeaturesNav feature={feature}/>
            <Container>
                <div className="sippy-container sippy-container-column">
                <Title title="Web Callback Module"/>
                <SVG element={WebCallbackModule} width={25} />
                <Paragraph size="medium" text='The Sippy Callback Module provides a mechanism to initiate a phone call using the web-browser, email client or mobile device of the user.  The Callback (or "Call-back") solution works either as a "Click-to-Call" function on a website (for example a retail site may have a "Click-to-Call" button on the website, so that website users can easily converse with the retail sites agents).   Web Callback uses many of the same features found in the Sippy Calling Card Module, for real-time call authorization and charging, dynamic call routing, and advanced call control.  Through the Sippy administrative portal, it is easy to configure access numbers, create IVR applications, add tariffs and perform monitoring & troubleshooting.'/>
                <Paragraph size="medium" text="In addition to the web callback, it is possible to set up a single (or multiple international) telephone numbers that the client can call.  The client is automatically registered using the telephone number (such as the mobile phone number), and is given a dial-tone.  The client enters the number that they wish to call, and hangs up.   The Sippy Softswitch will then complete the phone call to the destination number, and will also create a second phone call, back (hence, call-back) to the client - once answered, the client and the destination number will be connected together."/>
                <Paragraph size="medium" text='In addition to the account and reseller, web-portals allow users to re-review call history data, perform top-up and much more, Sippy provides a complete set of APIs allowing Sippy Customers to build "Click-to-Call" and other "directory" solutions using the Web Callback Module'/>
                <Paragraph size="medium" text="** IMPORTANT:  Callback solutions are prohibited in many countries worldwide.   Please ensure you have the necessary permits or licenses required, or the legal right to offer this service. "/>
                <ul className="sippy-list">
                    <li>
                        <span>Web based configuration</span>
                    </li>
                    <li>
                        <span>Comprehensive IVR’s</span>
                    </li>
                    <li>
                        <span>Supported languages</span>
                        <ul>
                            <li>Arabic, Armenia, Chinese, English, French, German, Japanese, Russian, Thai, Turkish, Vietnamese</li>
                        </ul>
                    </li>
                    <li>
                        <span>Recharge Vouchers</span>
                        <ul>
                            <li>Voucher usage Reporting</li>
                            <li>Credit/Voucher expiration</li>
                            <li>Unlimited Bulk generation</li>
                        </ul>
                    </li>
                    <li>
                        <span>PIN management (Account/Card management)</span>
                        <ul>
                            <li>Flexible PIN/Account definition</li>
                            <li>Bulk generation</li>
                            <li>Personal portal</li>
                        </ul>
                    </li>
                    <li>
                        <span>Pin or Pinless Dialing</span>
                    </li>
                    <li>
                        <span>Voicemail</span>
                    </li>
                    <li>
                        <span>Hotkeys</span>
                    </li>
                    <li>
                        <span>Trusted Number Authentication</span>
                    </li>
                    <li>
                        <span>Call Forwarding</span>
                    </li>
                    <li>
                        <span>API Accessibility</span>
                    </li>
                    <li>
                        <span>Account manipulation</span>
                    </li>
                    <li>
                        <span>Payments, credits, debits</span>
                    </li>
                    <li>
                        <span>Card Lifetime supported</span>
                    </li>
                    <li>
                        <span>Invoicing</span>
                    </li>
                    <li>
                        <span>Charges</span>
                    </li>
                    <li>
                        <span>Prefix based Minute plans</span>
                    </li>
                    <li>
                        <span>Mystery minutes</span>
                    </li>
                    <li>
                        <span>Post call surcharges</span>
                    </li>
                    <li>
                        <span>DID management</span>
                    </li>
                </ul>
                </div>
           </Container>
            <Footer/>
            </>
        )
    }
    else if (feature === 'did-management') {
        return(
            <>
            <FeaturesNav feature={feature}/>
            <Container>
                <div className="sippy-container sippy-container-column">
                <Title title="DID Management"/>
                <SVG element={DIDManagement} width={25} />
                <Paragraph size="medium" text='Direct Inbound Dialling number (Phone Numbers or "DIDs") offer a cost-effective way to extend your VoIP retail service of ITSP (Internet Telephony Service Provider) beyond your native area code.  With Sippy Software, you can easily manage your DIDs directly from the administrative or reseller portals, via the Sippy APIs, or you can bulk upload DIDs directly.   DIDs can have different buying/selling charges based on your business needs and are easily accounted for within the CDR reporting tools. '/>
                <Paragraph size="medium" text='Perfect for any standard Business Use as well as Call Center, Polling, Marketing, and Custom Application'/>
                <ul className="sippy-list">
                    <li>
                        <span>DID inventory search/filtering</span>
                    </li>
                    <li>
                        <span>Bulk upload tool</span>
                    </li>
                    <li>
                        <span>Flexible Inbound routing</span>
                        <ul>
                            <li>Route to client PBXs by IP addresses - Trunking</li>
                            <li>Follow me ( Call Forwarding ) style services where inbound call can be routed back out to an alternate destination</li>
                        </ul>
                    </li>
                    <li>
                        <span>Buying/Selling charges support</span>
                    </li>
                    <li>
                        <span>DID Status and Assignment visibility</span>
                    </li>
                    <li>
                        <span>DID CDR Reporting</span>
                    </li>
                </ul>
                </div>
            </Container>
            <Footer/>
            </>
        )
    }
    else if (feature === 'do-not-call-list') {
        return(
            <>
            <FeaturesNav feature={feature}/>
            <Container>
                <div className="sippy-container sippy-container-column">
                <Title title="Do not call list"/>
                <SVG element={DONOTCALLLIST} width={25} />
                <Paragraph size="medium" text='For Call Centers, Telemarketers or wholesale customers carrying large amounts of call center traffic,  you need to be aware of your responsibilities for using Do Not Call Lists (DNCL). Many countries and territories have implemented extensive "SPAM" and privacy laws that can result in the wholesale company being responsible for delivering unsolicited calls or faxes to paying substantial fines.'/>
                <Paragraph size="medium" text='There are several "National DNCL", "International" and Pan-European DNCL filters/lists available, to enable you to demonstrate that you are working to ensure that consumers who do not want to receive unsolicited calls, do not.  By implementing the DNCL filters, you ensure that the households you do contact are more likely to be receptive to receiving the call,  streamlining your calling process and potentially improving overall response rates and costs-per-call.'/>
                <Paragraph size="medium" text="While it is possible to implement one-time (or several, one-time) number blocking, the DNCL is the only option available to ensure that you maintain a complete list, and are compliant with new and changing regulatory laws concerning SPAM calling."/>
                <Paragraph size="medium" text='For more information, please go to:'/> <span><a href="https://support.sippysoft.com/solution/articles/3000041461-do-not-call-list-dnc-dncl-" target="_blank">Sippy DNCL Support Article</a></span>
                </div>
            </Container>
            <Footer/>
            </>
        )
    }
    else if (feature === 'business-reporting') {
        return(
            <>
            <FeaturesNav feature={feature}/>
            <Container>
                <div className="sippy-container sippy-container-column">
                <Title title="Business reporting"/>
                <SVG element={BusinessReporting} width={25} />
                <Paragraph size="medium" text="The Sippy Standby license is a secondary instance of the Sippy Softswitch that can be promoted to primary production server in the event of failure, adding a functional layer of network redundancy to enhance the operational stability of all voice network architectures. The Standby server can also be promoted to the primary role during scheduled maintenance, allowing for continuity of service. Achieve active failover, passive failover or data replication for reporting purposes, through the implementation of the Sippy Standby license."/>
                <ul className="sippy-list">
                    <li>
                        <span>'Real-time' time series graphs</span>
                        <ul>
                            <li>Call Processing time - Time to Vendor, SIP, LRN, AA.</li>
                            <li>Calls Per Second - Ingress cps, Egress cps, Authorized, Routed,Connected.</li>
                            <li>In Progress Calls / Concurrent Calls (Channels) - Total, Connected</li>
                        </ul>
                    </li>
                    <li>
                        <span>System-Wide ASR / ACD - ACD, ASR all in, ASR all out, ASR authenticated.</span>
                        <ul>
                            <li>Bandwidth In Use</li>
                            <li>Throughput</li>
                        </ul>
                    </li>
                    <li>
                        <span>Active Calls Monitoring</span>
                    </li>
                    <li>
                        <span>Real-time Credit/Balance reconciliation</span>
                    </li>
                    <li>
                        <span>Real-time call rating and billing</span>
                    </li>
                    <li>
                        <span>Network packet capture abilities - ability to capture network traffic for off-line analysis in wireshark or other industry standard tools.</span>
                    </li>
                </ul>
                </div>
            </Container>
            <Footer/>
            </>
        )
    }
    else if (feature === 'turkish-regulatory-features') {
        return(
            <>
            <FeaturesNav feature={feature}/>
            <Container>
            <Title title="For American and Canadian users"/>
                <div className="sippy-container flex-row">
                
                <Paragraph size="small" text="STIR/SHAKEN Funtionality is currently being developed by our engineering team. Expected completion of development is the end of 2020. The funtionality will be available for all new users and supports paying customers upon FREE upgrade and availability."/>
                <SVG element={TurkishRegulatoryFeatures} width={30} />
                 </div>
                 <hr className="sippy-hr-color"></hr>
                 <Title title="For Turkish users"/>
                 <div className="flex flex-row w-100-l">
                        <div className="mr4 ">
                        <Paragraph alignment="left" size="medium" text="Telekomünikasyon İletişim Başkanlığı (TİB) Reporting module."/>
                        <Paragraph alignment="left" size="medium" text="STH'lar için eksiksiz çözüm sunan SIP Platformu."/>
                        </div>
                        <div className="pl6">
                        <Paragraph size="medium" text="Developed and managed in partnership with "/>
                        <SippyImage src={Partner4} alt="partner 4" />
                        </div>
                 </div>
                 <div className="mt3">
                 <Paragraph size="medium" text="Sippy holds our Turkish friends in very high regard. We have developed a partnership with Eurotel, to offer a fully compliant module capable of keeping you on top of the changing VoIP business legislation imposed by BTK and TİB. We're working to continue to support the success of your business and importantly, the sensitivity of your information."/>
                 
                 </div>
            </Container>
            <Footer/>
            </>
        )
    }
    else if (feature === 'list-of-supported-currencies') {
        return(
            <>
            <FeaturesNav feature={feature}/>
            <Container>
                <div className="sippy-container sippy-container-column">
                <Title title="List of Supported Currencies"/>
                <SVG element={ListofSupporteCurrencies} width={25} />
                <Paragraph size="medium" text='Sippy Softswitch currently supports 170 global currencies and can be configured to adopt any as the base currency of your operation with easy conversion from any other currency supported (see below).  With tools such as "Loss Protection," Sippy protects from unfavorable exchange rates by allowing you to set a minimum margin for all transactions and limiting any exposure to adverse exchange rates.'/>
                <Paragraph size="medium" text="Please use the searchable table below to find specific currency options:"/>
                </div>
                
            </Container>
            <Footer/>
            </>
        )
    }
    else {
        return(
            <Container>
                <p>Ups. 404.</p>
            </Container>
        )
    }
}

export default function InnerFeature(props) {
    const feature = props.match.params.id;
    return ShowFeature(feature);
}