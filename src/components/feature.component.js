import React from 'react';
import styled from 'styled-components';
import Button from '@material-ui/core/Button';
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn';
import DesktopWindowsIcon from '@material-ui/icons/DesktopWindows';
import ListAltIcon from '@material-ui/icons/ListAlt';
import MemoryIcon from '@material-ui/icons/Memory';
import SVG from './svg.component';
import { ReactComponent as Redundancy } from '../assets/svg/redundancy.svg';
import { ReactComponent as BusinessReporting } from '../assets/svg/business-reporting.svg';
import { ReactComponent as flexiblerouting } from '../assets/svg/flexible-routing.svg';
import { ReactComponent as realtime } from '../assets/svg/real-time.svg';
import { ReactComponent as BillingManagement } from '../assets/svg/billing-management.svg';
import { ReactComponent as systemadministration } from '../assets/svg/system-administration.svg';
import { ReactComponent as xmlrpc } from '../assets/svg/xmlrpc.svg';
import { ReactComponent as SupportandSecurity } from '../assets/svg/support-and-security.svg';
import { ReactComponent as SippyEnvironments } from '../assets/svg/sippy-environments.svg';
import { ReactComponent as callingcardmodule } from '../assets/svg/calling-card-module.svg';
import { ReactComponent as WebCallbackModule } from '../assets/svg/web-call-back.svg';
import { ReactComponent as DIDManagement } from '../assets/svg/did-management-tools.svg';
import { ReactComponent as DONOTCALLLIST } from '../assets/svg/do-not-call-list.svg';
import { ReactComponent as TurkishRegulatoryFeatures } from '../assets/svg/turkish.svg';
import { ReactComponent as ListofSupporteCurrencies } from '../assets/svg/currencies-supported.svg';

const FeatureContainer = styled.div`
    width: 100%;
    padding: 18px;
    text-align: center;
    margin-right: 20px;
    border-radius: 4px;
    transition: .2s all ease;
    cursor: pointer;
    &:hover {
        background: #333333;
    }
    &:last-child {
        margin-right: 0;
    }
    & svg {
        width: 60px;
        height: 60px;
        margin-bottom: 12px;
        color: #ec8d2f;
    }
    & h1 {
        font-weight: 500;
        font-size: 18px;
        margin-bottom: 32px;
        color: white;
    }
    & a {
        color: #ec8d2f;
        border-color: #ec8d2f;
    }
`

export default function Feature(props) {
    const title = props.title;
    const description = props.description;
    const icon = props.icon;
    const readMoreLabel = props.readMoreLabel;
    const route = props.route;
    return(
        <FeatureContainer>
            <a href={'features/' + route} variant="outlined" size="small">
            {
                icon === "Business-Reporting"
                ?
                <SVG element={BusinessReporting} width={25} />
                :
                icon === "Real-Time"
                ?
                <SVG element={realtime} width={25} />
                :
                icon === "flexible-routing-tools"
                ?
                <SVG element={flexiblerouting} width={25} />
                :
                icon === "Billing-Management"
                ?
                <SVG element={BillingManagement} width={25} />
                :
                icon === "System-Administration"
                ?
                <SVG element={systemadministration} width={25} />
                :
                icon === "xml-rpc"
                ?
                <SVG element={xmlrpc} width={25} />
                :
                icon === "Support-and-Security"
                ?
                <SVG element={SupportandSecurity} width={25} />
                :
                icon === "Sippy-Environments"
                ?
                <SVG element={SippyEnvironments} width={25} />
                :
                icon === "Calling-Card-Module"
                ?
                <SVG element={callingcardmodule} width={25} />
                :
                 icon === "Web-Callback-Module"
                ?
                <SVG element={WebCallbackModule} width={25} />
                :
                icon === "DID-Management"
                ?
                <SVG element={DIDManagement} width={25} />
                :
                icon === "DO-NOT-CALL-LIST"
                ?
                <SVG element={DONOTCALLLIST} width={25} />
                :
                icon === "Turkish-Regulatory-Features"
                ?
                <SVG element={TurkishRegulatoryFeatures} width={25} />
                :
                icon === "List-of-Supported-Currencies"
                ?
                <SVG element={ListofSupporteCurrencies} width={25} />
                :
                null
            }
            <h1>{title}</h1>  
            </a>          
        </FeatureContainer>
    )
}