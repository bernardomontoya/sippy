import React from 'react';
import styled from 'styled-components';

const StyledReadMore = styled.button`
    background: #FFF;
    border: 2px solid #ed9122;
    border-radius: 60px;
    padding: 14px;
    width: 140px;
    font-size: 14px;
    font-weight: 700;
    color: #ed9122;
`

export default function ReadMore(props) {
    const text = props.text;
    return(
    <StyledReadMore>{text}</StyledReadMore>
    )
}