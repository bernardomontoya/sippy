import React from 'react';
import styled from 'styled-components';
import Title from './title.component';
import Paragraph from './paragraph.component';
import SippyButton from './sippy-button.component';
import Footer from './footer.component';
import { device } from './devices';
const Container = styled.div`
    width: 100%;
    max-height: 200%;
    padding: 60px 40px 80px;
    display: flex;
    flex-direction: column;
    align-items: center;
    & .sippy-contact-us-text-container {
        margin-bottom: 40px;
    }
    & .sippy-list {
        color: #989898;
        margin: 0;
        margin-bottom: 60px;
        font-size: 20px;
        width: 100%;
        & li {
            margin-bottom: 8px;
            &:last-child {
                margin-bottom: 0;
            }
        }
    }
`
const StyledTitle = styled.h1`
    font-size: 30px;
    color: #f8941e;
    @media (max-width: ${device.mobileL}) {
        font-size: 32px;
    }
`
const TitleContainer = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: left;
    justify-content: left;
    text-align: left;
    margin-bottom: 40px;
    & span {
        color: #ec8d2f;
        margin-top: 8px;
        font-size: 20px;
    }
`
export default function ContactUs() {
    return(
        <>
        <Container>
            <div className="sippy-container sippy-container-column">
                <div className="sippy-contact-us-text-container">
                    <Paragraph size="medium" text="Sippy Software Inc. is a corporation legally registered under the laws of Canada, we abide by
the rules and we execute our affairs ethically. Our good reputation has been built over many
years. Sippy’s ownership and management team is made up of well known industry
professionals who regularly contribute efforts to improve our industry."/>
                    <Paragraph size="medium" text="There are companies that steal the work we do and that illegally capitalize on the name we have
made, these companies steal from us and they sell cheap, stolen, software in order to steal from
you (routes, customers, private information, etc.)."/>
                    <Paragraph size="larger" alignment="center" color="#ec8d2f" text="BEWARE!!!"/>
                    <Paragraph alignment="left" size="medium" text="The worst known offender for Fraud, Piracy and Theft, is a company that goes by the
misleading name: SIPPY SWITCH"/>
                    <Paragraph size="medium" alignment="left" text="You may find Sippy Switch on social media, and online. DO NOT TRUST THEM"/>
                    <Paragraph size="medium" alignment="left" text="Report these companies to the authorities to keep yourself and your customers safe."/>
                </div>
               
                <SippyButton label="REPORT FRAUD" hiper="https://reporting.bsa.org/r/report/add.aspx?src=ca+ln=en-ca+cmpid=CA_GA_ENG_GOOG_PiratedSoftware+gclid=CjwKCAiAy9jyBRA6EiwAeclQhEag6kBEvCtCFW6hmrWmegK3If_2bH1YO2UJ3az65yvMUjmNxWV0GhoC_NsQAvD_BwE" size="larger"/>
               <br></br>
               <br/>
                <TitleContainer>
                <StyledTitle>SIPPYSOFT’S FRAUD RELIEF PROGRAM</StyledTitle>
                </TitleContainer>
                <Paragraph size="medium" alignment="left" text="If you or a company you know of purchased a license from the fraudulent company Sippy Switch (https://www.sippyswitch.co/) and they stole your money, customers, routes, etc., Sippy Software Inc. has great news for you!!!"/>
                <Paragraph size="medium" alignment="left" text="We are offering a relief plan whereby you can receive a discount based on the amount of money Sippy Switch (the fraudulent company) stole from you."/>
                <Paragraph size="medium" alignment="left" text="Here is how the Sippy Switch Fraud Relief Program works:"/>
                <ul className="sippy-list">
                    <li>
                        <span>You provide us details of your dealings with Sippy Switch (the fraudulent company) along with details about your switch</span>
                    </li>
                    <li>
                        <span>We review the information you share and do an internal review</span>
                    </li>
                    <li>
                        <span>If our internal review is positive, you will receive a notification</span>
                    </li>
                    <li>
                        <span>We provide you a credit towards the rental or purchase of a REAL license with our company</span>
                    </li>
                </ul>
                <Paragraph size="medium" alignment="left" text="Email our sales team at sales@sippysoft.com and tell us about your negative experience with Sippy Switch the fraudulent company and we will do our best to provide help."/>
                <Paragraph size="medium" alignment="left" text="As Sippy Switch (the fraudulent company) is well known to run scams in our industry, we need to be very careful with how we issue the Fraud Relief Program credits so we retain the right to refuse any requests under the Fraud Relief Program."/>
            </div>
        </Container>
        <Footer/>
        </>
    )
}