import React from 'react';
import Button from '@material-ui/core/Button';
import styled from 'styled-components';
import Title from './title.component';
import Paragraph from './paragraph.component';
import Value from './value.component';
import IntroSection from './intro-section.component';
import SippyImage from './image.component';
import AboutVideo from '../assets/videos/about.mp4'
import { device } from './devices';
import SVG from './svg.component';
import { ReactComponent as Timeline } from '../assets/svg/timeline.svg';
import { ReactComponent as InvestInSippy } from '../assets/svg/invest-in-sippy.svg';
import Partner1 from '../assets/images/partner1.JPG';
import Partner2 from '../assets/images/partner2.JPG';
import Partner3 from '../assets/images/partner3.JPG';
import Partner4 from '../assets/images/partner4.JPG';
import Partner5 from '../assets/images/partner5.JPG';
import Partner6 from '../assets/images/partner6.JPG';
import TimelineP from '../assets/images/timeline.png';
import Footer from './footer.component';
import SippyButton from './sippy-button.component';
import { ReactComponent as Github } from '../assets/svg/github.svg';
const Container = styled.div`
    width: 100%;
    padding: 0 40px;
    display: flex;
    flex-direction: column;
    align-items: center;
    padding-bottom: 80px;
    & .sippy-about-title {
        height: 100px;
        display: flex;
        align-items: center;
        justify-content: center;
        text-align: center;
    }
    & .sippy-about-values {
        display: flex;
        flex-direction: row;
        justify-content: center;
        align-items: center;
        margin-bottom: 80px;
        @media (max-width: ${device.mobileL}) {
            flex-direction: column;
            & > div {
                width: 100%;
                margin-right: 0;
                margin-bottom: 60px;
                &:last-child {
                    margin-bottom: 0;
                }
            }
        }
    }
    & .sippy-about-partners-container {
        margin-bottom: 40px;
    }
    & .sippy-about-partner-row {
        width: 100%;
        display: flex;
        flex-direction: row;
        margin-bottom: 12px;
        &:last-child {
            margin-bottom: 0;
        }
        & .sippy-about-partner {
            background: #FFF;
            border-radius: 5px;
            height: 200px;
            width: 100%;
            display: flex;
            margin-right: 12px;
            &:last-child {
                margin-right: 0;
            }
        }
    }
`
const ItemButton = styled.a`
    width: 80px;
}
`


export default function About() {
    return(
        <>
        <Container>
            <div className="sippy-container sippy-container-column">
                <IntroSection type="video" hasButton={false} video={AboutVideo} />
                <div className="sippy-about-values">
                    <Value text="Global Carrier Customers" value="600+" percentage={true}/>
                    <Value text="International Annual Phone Calls" value="750 Million" percentage={false}/>
                    <Value text="Minutes of Telecom Calls" value="25 Billion" percentage={false}/>
                </div>
                <Title title="Sippy Software Inc."/>
                <Paragraph size="medium" text="As a company, Sippy has been a leading provider of reliable, profitable, and intuitive global telecommunications solutions for Internet Telephony worldwide since 2004. Our scalable, innovative VoIP products and solutions are architectured by a development team that has focused on revolutionizing telecommunications, delivering a cost-effective communication architecture with operating costs that cannot be matched by any traditional circuit-switched PSTN network provider worldwide. Sippy currently handles more than 4 billion minutes of traffic, across thousands of client networks in more than 100 countries worldwide, assisting new ITSPs to launch on a weekly basis and contributing significant advancements to many open-source SIP projects."/>
                <SippyImage src={TimelineP} width={100} alt="partner 1" />
                <Title title="Reputation"/>
                <Paragraph size="medium" text="Sippy Software, Inc. has established itself as a leader in the design and development of next-generation, high-performance solutions for VoIP communication systems.  Our customers demand that we fulfill promises and that’s what we do; from adding the features they request to following through on our support guarantees.  We work hard to earn our reputation everyday."/>
                <Title title="Expertise"/>
                <ItemButton target="_blank" href="https://github.com/sippy"><SVG element={Github} width={100} /> </ItemButton>
                <Paragraph size="medium" text="Sippy strongly believes that open source and open standards represent the only future for the telecoms industry. We actively contribute major fixes and enhancements to many well-known open-source projects such as Asterisk, SIP Express Router, OpenSIPS, Vovida and FreeBSD. We also actively believe that we should not compete against our clients, and so remain solely focused on developing core, leading edge, Class 4 and 5 switch technologies."/>
                <Title title="Partners"/>
                <Paragraph size="medium" text="Sippy Partners represent the Sippy commitment to value, technology, trust, and reliability.  Sippy partners consistently offer the most economical, and best support Sippy Software solutions on the market."/>
                <div className="sippy-about-partners-container">
                    <div className="sippy-about-partner-row">
                        <div className="sippy-about-partner">
                            <SippyImage src={Partner1} alt="partner 1" />
                        </div>
                        <div className="sippy-about-partner">
                            <SippyImage src={Partner2} alt="partner 2" />
                        </div>
                        <div className="sippy-about-partner">
                            <SippyImage src={Partner3} alt="partner 3" />
                        </div>
                    </div>
                    <div className="sippy-about-partner-row">
                        <div className="sippy-about-partner">
                            <SippyImage src={Partner4} alt="partner 4" />
                        </div>
                        <div className="sippy-about-partner">
                            <SippyImage src={Partner5} alt="partner 5" />
                        </div>
                        <div className="sippy-about-partner">
                            <SippyImage src={Partner6} alt="partner 6" />
                        </div>
                    </div>
                </div>
                <Paragraph size="medium" text="Before doing business with anyone or any company claiming to be a Sippysoft Partner, please check with us. Unfortunately, the market is filled with pirated (extremely old) versions of our software, and there are companies out there claiming to be either Sippy or Sippy partners, beware.  There is only one Sippy, and only one Sippy Softswitch!"/>
                <Paragraph size="medium" text="Just as our partners deliver quality and are industry leaders, our Authorized Resellers are companies and individuals who have been recognized by our team as possessing technical or business qualities that are key to our efforts for reaching our market and helping to provide an initial point of contact for sales related inquiries."/>
                <Paragraph size="medium" text="Click on the link to see an up to date list of our certified resellers:"/>
                <SippyButton label="Authorized Resellers " hiper="https://sippysoft.agilecrm.com/landing/5514217950085120" size="larger"/>
                <br/>
                <Paragraph size="medium" text="If you do not see the name of the company or person you’re dealing with on the list then he is not a certified Sippy Software reseller.  We update the list of resellers once per week so you can trust that the information contained in it is accurate and current."/>
                <Paragraph size="medium" text="Want to be a reseller?"/>
                <Paragraph size="medium" text="Contact us and let us know about your business, your experience and how you see yourself reselling our softswitch; email sales@sippysoft.com"/>
                <Title title="Direction"/>
                <Paragraph size="medium" text="The evolution of VoIP communications hasn’t stopped; we believe that with the advent of 5G and the technologies that are sure emerge from it, SIP signalling will become more and more critical which is why our growth plans are designed to keep ahead of the technology curve and to serve the needs of our user base as they grow they grow."/>
                <Title title="Invest in Sippy Software"/>
                <SVG element={InvestInSippy} width={25} />
                <Paragraph size="medium" text="Our management team is highly experienced and very well connected, our technical and sales staff is professional and highly qualified.  We are conservative with the way we plan but aggressive with the way we execute, if you are interested in an investment opportunity with a company that sets the mark in technology and constantly performs to high expectations then get in contact with us and find out how you can invest with us."/>
            </div>
        </Container>
        <Footer/>
        </>
    )
}