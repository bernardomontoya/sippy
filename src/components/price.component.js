import React from 'react';
import styled from 'styled-components';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';

function BuildPriceFeatures(features) {
    console.log(features);

}

export default function PriceCard(props) {
    // function props
    const currencySymbol = props.currencySymbol;
    const configuration = props.configuration;
    const title = configuration.title;
    const price = configuration.price;
    const cents = configuration.cents;
    const description = configuration.description;
    const hasDetails = configuration.hasDetails;
    const detailsLabel = configuration.detailsLabel;
    const billedAnnuallyLabel = configuration.billedAnnuallyLabel;
    const billedMonthlyLabel = configuration.billedMonthlyLabel;
    const hasAction = configuration.hasAction;
    const actionLabel = configuration.actionLabel;
    const backgroundColor = configuration.color;
    const textColor =  configuration.textColor;
    const priceIncludesLabel = configuration.priceIncludesLabel;
    const priceFeatures = configuration.features;
    // helper functions
    const BuildPriceFeatures = priceFeatures.map(function(feature, i) {
        return(
            <li key={i}>{feature.feature}</li>
        )
    });
    // styles
    const PriceCardContainer = styled.div`
        width: 100%;
        max-width: 325px;
        margin-right: 52px;
        text-align: center;
        display: flex;
        flex-direction: column;
        &:last-child {
            margin-right: 0;
        }
        & .sippy-price-card {
            width: 100%;
            height: 360px;
            padding: 20px;
            margin-bottom: 32px;
            display: flex;
            flex-direction: column; 
            align-items: center;
            position: relative;
            color: ${textColor} !important;
            background: ${backgroundColor} !important;
            & .sippy-price-card-title {
                text-align: center;
                font-size: 14px;
                margin-bottom: 8px;
            }
            & .sippy-price-card-price {
                text-align: center;
                display: flex;
                flex-direction: row;
                justify-content: center;
                align-items: center;
                margin-bottom: 8px;
                & .sippy-price-card-price-symbol {
                    font-size: 30px;
                    margin-right: 4px;
                }
                & .sippy-price-card-price-main {
                    font-size: 60px;
                }
                & .sippy-price-card-price-cents {
                    font-size: 20px;
                    margin-left: 4px;
                    margin-top: -14px;
                }
            }
            & .sippy-price-card-text {
                display: flex;
                flex-direction: column;
                margin-bottom: 14px;
                & > div {
                    margin-bottom: 8px;
                    &:last-child {
                        margin-bottom: 0;   
                    }
                }
            }
            & .sippy-price-card-details {
                width: fit-content;
                background: none;
                border: none;
                color: #FFF;
                font-size: 14px;
                text-decoration: underline;
                cursor: pointer;
            }
            & .MuiButton-root {
                color: #FFF;
                background-color: rgba(0, 0, 0, 0.3);
                position: absolute;
                bottom: 20px;
            }
        }
        .sippy-price-details {
            text-align: left;
            .sippy-price-details-includes-header {
                color: ${backgroundColor};
                & h2 {
                    font-size: 16px;
                    margin-bottom: 6px;
                }
                & p {
                    font-size: 16px;
                }
                & .sippy-price-details-includes-separator {
                    width: 100%;
                    height: 2px;
                    background: ${backgroundColor};
                    margin-top: 14px;
                    margin-bottom: 14px;
                }
            }
            & .sippy-price-details-includes-features ul {
                padding-left: 26px;
                font-size: 14px;
                color: #afafaf;
            }
        }
    `

    return(
        <PriceCardContainer>
            <Paper className="sippy-price-card" elevation={3}>
                <div className="sippy-price-card-title">
                    <h1>{title}</h1>
                </div>
                <div className="sippy-price-card-price">
                    <span className="sippy-price-card-price-symbol">{currencySymbol}</span>
                    <span className="sippy-price-card-price-main">{price}</span>
                    { cents === "00" ? null : <span className="sippy-price-card-price-cents">.{cents}</span> }
                </div>
                <div className="sippy-price-card-text">
                    { description === "" ? null : 
                    <div className="sippy-price-card-description">
                        <span>{description}</span>
                    </div>
                    }
                    { billedAnnuallyLabel === "" ? null : 
                    <div className="sippy-price-card-billed">
                        <span>{billedAnnuallyLabel}</span>
                    </div>
                    }
                    { billedMonthlyLabel === "" ? null : 
                    <div className="sippy-price-card-billed">
                        <span>{billedMonthlyLabel}</span>
                    </div>
                    }
                </div>
                { !hasDetails ? null : 
                <button className="sippy-price-card-details">
                    <span>{detailsLabel !== "" ? detailsLabel: "Details"}</span>
                </button>
                }
                { !hasAction ? null : 
                <Button variant="contained" size="small">
                    {actionLabel !== "" ? actionLabel: "Action"}
                </Button>
                }
            </Paper>
            <div className="sippy-price-details">
                <div className="sippy-price-details-includes">
                    <div className="sippy-price-details-includes-header">
                        <h2>{title} includes</h2>
                        <p>{priceIncludesLabel}</p>
                        <div className="sippy-price-details-includes-separator"></div>
                    </div>
                    <div className="sippy-price-details-includes-features">
                        <ul>
                            {BuildPriceFeatures}
                        </ul>
                    </div>
                </div> 
            </div>
        </PriceCardContainer>
    )
}
