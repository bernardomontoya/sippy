import React from "react";
import styled from "styled-components";
import Logo from "../assets/images/logo_footer.png";
import TextField from "@material-ui/core/TextField";
import IconButton from "@material-ui/core/IconButton";
import FacebookIcon from "@material-ui/icons/Facebook";
import InstagramIcon from "@material-ui/icons/Instagram";
import YouTubeIcon from "@material-ui/icons/YouTube";
import TwitterIcon from "@material-ui/icons/Twitter";
import LinkedInIcon from "@material-ui/icons/LinkedIn";
import GitHubIcon from "@material-ui/icons/GitHub";
import SippyButton from "./sippy-button.component";
import { device } from "./devices";

const FooterContainer = styled.div`
  width: 100%;
  height: 200px;
  padding: 40px;
  display: flex;
  flex-direction: row;
  background: #333333;
  justify-content: center;
  & .sippy-container-wrapper {
    width: 100%;
    display: flex;
    @media (max-width: ${device.mobileL}) {
      flex-direction: column;
    }
  }
  & .sippy-footer-logo-container {
    display: flex;
    flex-direction: column;
    height: 100%;
    align-items: center;
    justify-content: center;
    margin-right: 60px;
    & img {
      width: 150px;
      margin-bottom: 8px;
      max-width: none;
    }
    & span {
      color: #afafaf;
    }
    @media (max-width: ${device.mobileL}) {
      margin-right: 0;
    }
  }
  & .sippy-footer-container {
    display: flex;
    flex-direction: column;
    width: 100%;
    margin-right: 60px;
    &:last-child {
      margin-right: 0;
    }
    & span {
      color: #afafaf;
    }
    & button {
      color: #afafaf;
    }
    .sippy-footer-places {
      font-size: 14px;
    }
    #sippy-subscribe-label {
      color: #afafaf;
    }

    .MuiInput-underline:before {
      border-color: #afafaf;
    }
    .MuiInput-underline:after {
      border-color: #ec8d2f;
    }
    .MuiInput-underline:hover:not(.Mui-disabled):before {
      border-color: #afafaf;
    }
    & input {
      box-sizing: content-box;
    }
  }
  & .sippy-footer-contact-title {
    color: #afafaf;
    font-size: 16px;
    margin-bottom: 12px;
  }
  @media (max-width: ${device.mobileL}) {
    flex-direction: column;
    height: auto;
    margin-top: 40px;
    .sippy-footer-text {
      display: none;
    }
    .sippy-footer-social-links {
      display: flex;
      justify-content: space-between;
      margin: 20px 0;
    }
  }
`;

export default function Footer(props) {
  const currentYear = new Date().getFullYear();
  return (
    <FooterContainer>
      <div className="sippy-container">
        <div className="sippy-container-wrapper">
          <div className="sippy-footer-logo-container">
            <img src={Logo} alt="sippy" />
            <span>{currentYear}</span>
          </div>
          <div className="sippy-footer-container sippy-footer-text">
            <span className="sippy-footer-contact-title sippy-footer-text">
              Contact
            </span>
            <div className="sippy-footer-places">
              <div>
                <span>8100 Winston Street Burnaby, BC, Canada V5A 2H5</span>
              </div>
              <div>
                <div>
                  <span>Toll Free: +1 855 747 7779</span>
                </div>
                <div>
                  <span>Canada: +1 778 783 0474</span>
                </div>
                <div>
                  <span>Support: 24 x 7 - 365 days</span>
                </div>
              </div>
            </div>
          </div>
          <div className="sippy-footer-container">
            <span className="sippy-footer-contact-title sippy-footer-text">
              Follow us
            </span>
            <div className="sippy-footer-social-links">
              <IconButton
                target="_blank"
                aria-label="facebook"
                size="medium"
                href="https://www.facebook.com/SippySoft/"
              >
                <FacebookIcon fontSize="medium" />
              </IconButton>
              <IconButton
                target="_blank"
                aria-label="youtube"
                size="medium"
                href="https://www.youtube.com/user/sippysoft/videos"
              >
                <YouTubeIcon fontSize="medium" />
              </IconButton>
              <IconButton
                target="_blank"
                aria-label="twitter"
                size="medium"
                href="https://twitter.com/sippysoft"
              >
                <TwitterIcon fontSize="medium" />
              </IconButton>
              <IconButton
                target="_blank"
                aria-label="linkedin"
                size="medium"
                href="https://www.linkedin.com/company/sippysoft/"
              >
                <LinkedInIcon fontSize="medium" />
              </IconButton>
              <IconButton
                target="_blank"
                aria-label="GitHub"
                size="medium"
                href="https://github.com/sippy"
              >
                <GitHubIcon fontSize="medium" />
              </IconButton>
            </div>
          </div>
          <div className="sippy-footer-container sippy-footer-text">
            <SippyButton
              label="Privacy Terms"
              hiper="https://drive.google.com/file/d/1AfHkvihhW7v42gXQf-fakc0Vkx17C2Fu/view"
              size="small"
            />
          </div>
        </div>
      </div>
    </FooterContainer>
  );
}
