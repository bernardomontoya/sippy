import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { makeStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

// styles
const CalculatorContainer = styled.div`
    border: 2px solid white;
    border-radius: 4px;
    transition: .2s all ease;
`
const SelectControl = styled(Select)`
    color: white !important;
    & svg {
        color: white;
    }
    &.MuiInput-underline:before {
        border-color: white !important;
    }
`
const TotalInput = styled.input`
    background: transparent;
    border: none;
    border-bottom: 1px solid #fff;
    padding: 0;
    margin: 0;
    width: inherit;
    height: 34px;
    color: white;
    font-size: 22px;
    text-align: right;
    padding-bottom: 12px;
`


export default function Calculator() {
    // state
    const [ hostingType, setHostingType ] = useState("");
    const [ hostingOption, setHostingOption ] = useState(0);
    const [ setUpFee, setSetUpFee ] = useState(0);
    const [ total, setTotal ] = useState(0);

    // handle state
    const HandlingHostingType = (e) => {
        const value = e.target.value;
        let fee = 0;
        if (value === 'shared-hosted') {
            fee = 45;
        }
        else if (value === 'dedicated-premium' || value === 'self-hosted' || value === 'dedicated-standard') {
            fee = 200;
        }
        else {
            fee = 0;
        }
        setHostingOption(0);
        setSetUpFee(fee);
        setHostingType(value);
    }
    const HandleHostingOption = (e) => {
        setHostingOption(e.target.value);
    }
    return(
        <CalculatorContainer className="pa4 w-100 w-40-ns justify-center flex flex-column">
            <div className="flex flex-column flex-row-ns mb4">
                <h1 className="white f4 f3-ns w-100 tr-ns mr4-ns mb3 mb0-ns">Hosting:</h1>
                <FormControl className="w-100">
                    <SelectControl
                        labelId="hosting-type-select-label"
                        id="hosting-type-select"
                        value={hostingType}
                        onChange={HandlingHostingType}
                    >
                        <MenuItem value="shared-hosted">Shared Hosted</MenuItem>
                        <MenuItem value="dedicated-standard">Dedicated (standard)</MenuItem>
                        <MenuItem value="dedicated-premium">Dedicated (premium)</MenuItem>
                        <MenuItem value="self-hosted">Self Hosted</MenuItem>
                    </SelectControl>
                </FormControl>
            </div>
            <div className="flex flex-column flex-row-ns mb4">
                <h1 className="white f4 f3-ns w-100 tr-ns mr4-ns mb3 mb0-ns">Capacity:</h1>
                <FormControl className="w-100">
                    {
                        hostingType === "shared-hosted"
                        ?
                            <SelectControl
                                labelId="call-capacity-select-label"
                                id="call-capacity-select"
                                value={hostingOption}
                                onChange={HandleHostingOption}
                            >
                                <MenuItem value={195} >100 CC / 15 CPS</MenuItem>
                                <MenuItem value={295} >250 CC / 30 CPS</MenuItem>
                                <MenuItem value={495} >500 CC / 45 CPS</MenuItem>
                                <MenuItem value={695} >1000 CC / 60 CPS</MenuItem>
                                <MenuItem value={995} >2000 CC / 75 CPS</MenuItem>
                                <MenuItem value={1595} >4000 CC / 90 CPS</MenuItem>
                            </SelectControl>
                        :
                        hostingType === "dedicated-standard"
                        ?
                            <SelectControl
                            labelId="call-capacity-select-label"
                            id="call-capacity-select"
                            value={hostingOption}
                            onChange={HandleHostingOption}
                            >
                                <MenuItem value={476 + 153.45 }>2000 CC / 75 CPS</MenuItem>
                                <MenuItem value={576 + 199.95 }>3000 CC / 90 CPS</MenuItem>
                                <MenuItem value={676 + 199.95} >4000 CC / 105 CPS</MenuItem>
                                <MenuItem value={776 + 199.95} >5000 CC / 120 CPS</MenuItem>
                                <MenuItem value={876 + 199.95} >6000 CC / 135 CPS</MenuItem>
                                <MenuItem value={976 + 341} >7000 CC / 150 CPS</MenuItem>
                                <MenuItem value={1076 + 341} >8000 CC / 165 CPS</MenuItem>
                                <MenuItem value={1176 + 341} >9000 CC / 180 CPS</MenuItem>
                                <MenuItem value={1276 + 618.45} >10,000 CC / 195 CPS</MenuItem>
                                <MenuItem value={1376 + 618.45} >11,000 CC / 195 CPS</MenuItem>
                                <MenuItem value={1476 + 773.45} >12,000 CC / 195 CPS</MenuItem>
                                <MenuItem value={1576 + 773.45} >13,000 CC / 195 CPS</MenuItem>
                                <MenuItem value={1676 + 1393.45} >14,000 CC / 195 CPS</MenuItem>
                            </SelectControl>
                        :
                        hostingType === "dedicated-premium" || hostingType === "self-hosted"
                        ?
                            <SelectControl
                                labelId="call-capacity-select-label"
                                id="call-capacity-select"
                                value={hostingOption}
                                onChange={HandleHostingOption}
                            >
                                <MenuItem value={ hostingType === "dedicated-premium" ? (476 + 230.175) : 476 } >2000 CC / 75 CPS</MenuItem>
                                <MenuItem value={ hostingType === "dedicated-premium" ? (576 + 299.925) : 576 } >3000 CC / 90 CPS</MenuItem>
                                <MenuItem value={ hostingType === "dedicated-premium" ? (676 + 299.925) : 676 } >4000 CC / 105 CPS</MenuItem>
                                <MenuItem value={ hostingType === "dedicated-premium" ? (776 + 299.925) : 776 } >5000 CC / 120 CPS</MenuItem>
                                <MenuItem value={ hostingType === "dedicated-premium" ? (876 + 299.925) : 876 } >6000 CC / 135 CPS</MenuItem>
                                <MenuItem value={ hostingType === "dedicated-premium" ? (976 + 511.5) : 976 } >7000 CC / 150 CPS</MenuItem>
                                <MenuItem value={ hostingType === "dedicated-premium" ? (1076 + 511.5) : 1076 } >8000 CC / 165 CPS</MenuItem>
                                <MenuItem value={ hostingType === "dedicated-premium" ? (1176 + 511.5) : 1176 } >9000 CC / 180 CPS</MenuItem>
                                <MenuItem value={hostingType === "dedicated-premium" ? (1276 + 927.675) : 1276} >10,000 CC / 195 CPS</MenuItem>
                                <MenuItem value={hostingType === "dedicated-premium" ? (1376 + 927.675) : 1376} >11,000 CC / 195 CPS</MenuItem>
                                <MenuItem value={hostingType === "dedicated-premium" ? (1476 + 1160.175) : 1476} >12,000 CC / 195 CPS</MenuItem>
                                <MenuItem value={hostingType === "dedicated-premium" ? (1576 + 1160.175) : 1576} >13,000 CC / 195 CPS</MenuItem>
                                <MenuItem value={hostingType === "dedicated-premium" ? (1676 + 2090.175) : 1676} >14,000 CC / 195 CPS</MenuItem>
                            </SelectControl>
                        : null
                    }
                </FormControl>
            </div>
            <div className="flex flex-column flex-row-ns">
                <h1 className="white f4 f3-ns w-100 tr-ns mr4-ns mb3 mb0-ns">Total:</h1>
                <span className="w-100 white tr f3 fw7">$ {(hostingOption).toFixed(2)}</span>
            </div>
            <div class="w-100 white tc mt4">
            <p className="f6 mb2">
                Account install fee of ${setUpFee}.00 applies.
            </p>
            <p className="f6">
                {
                    hostingType === 'self-hosted'
                    ?
                    "Price includes first year support. Install fee is charged only on first invoice."
                    :
                    "All prices are monthly in USD. Install fee is charged only on first invoice."
                }
                </p>
            </div>
        </CalculatorContainer>
    )
}