import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Nav from './components/nav.component';
import Home from './components/home.component';
import About from './components/about.component';
import Products from './components/products.component';
import Product from './components/product.component';
import NotFound from './components/not-found.component';
import Prices from './components/prices.component';
import Features from './components/features.component';
import InnerFeature from './components/inner-feature.component';
import ContactUs from './components/contact-us.component';
import TechnicalInformation from './components/technical-information.component';
import ComparePrices from './components/compare-prices.component';
import fraud from './components/fraud.component';
import form from './components/form.component';

export default function Routes(props) {
    return(
        <>
            <Nav/>
            <Switch>
                <Route path="/" exact component={Home}/>
                <Route path="/about" component={About}/>
                <Route path="/products/:id" component={Product}/>
                <Route path="/products" component={Products}/>
                <Route path="/features/:id" component={InnerFeature}/>
                <Route path="/features" component={Features}/>
                <Route path="/technical-information" component={TechnicalInformation}/>
                <Route path="/prices/compare" component={ComparePrices}/>
                <Route path="/prices" component={Prices}/>
                <Route path="/contact-us" component={ContactUs}/>
                <Route path="/fraud" component={fraud}/>
                <Route path="/form" component={form}/>
                <Route component={NotFound}/>
            </Switch>
        </>
    )
}